<?php

namespace App\Enums;

enum PaymentTermsEnum: string
{
    case IMMEDIAT = 'immediat';
    case QUINZAINE = '15_days';
    case MENSUEL = '30_days';
    case BIMENSUEL = '60_days';
    case MENSUEL_END = 'end_next_month';
    case COMPLEXITY = 'complexity';
}
