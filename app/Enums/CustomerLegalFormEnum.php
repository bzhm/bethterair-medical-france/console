<?php

namespace App\Enums;

enum CustomerLegalFormEnum: string
{
    case Particulier = 'particulier';
    case Professionnel = 'professionnel';
    case Revendeur = 'revendeur';
    case Groupement = 'groupement';
}
