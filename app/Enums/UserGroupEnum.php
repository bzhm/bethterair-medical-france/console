<?php

namespace App\Enums;

enum UserGroupEnum: string
{
    case administrateur = 'administrateur';
    case client = 'client';
    case commercial = 'commercial';
    case revendeur = 'revendeur';
    case fournisseur = 'fournisseur';
    case technicien = 'technicien';
}
