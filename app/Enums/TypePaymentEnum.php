<?php

namespace App\Enums;

enum TypePaymentEnum: string
{
    case CREDIT_CARD_ONE_SHOT = 'credit_card_one_shot';
    case CREDIT_CARD_N_PAYMENT = 'credit_card_n_payment';
    case TRANSFERT = 'virement';
    case SEPA_DEBIT = 'prelevement_sepa';
}
