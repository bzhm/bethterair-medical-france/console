<?php

namespace App\Enums;

enum StockMvmTypeEnum: string
{
    case RECEPTION = 'reception';
    case LIVRAISON = 'livraison';
    case RETOUR = 'retour';
}
