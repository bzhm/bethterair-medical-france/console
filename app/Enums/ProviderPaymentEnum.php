<?php

namespace App\Enums;

enum ProviderPaymentEnum: string
{
    case stripe = 'Stripe';
    case alma = 'Alma';
}
