<?php

namespace App\Enums;

enum ArticleUnityEnum: string
{
    case unite = 'unite';
    case time_minute = 'min';
    case time_hour = 'hour';
    case time_day = 'day';
}
