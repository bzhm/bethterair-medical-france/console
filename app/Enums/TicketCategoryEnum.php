<?php

namespace App\Enums;

enum TicketCategoryEnum: string
{
    case INCIDENT = 'incident';
    case ASSISTANCE = 'assistance';
    case BILLING = 'billing';
}
