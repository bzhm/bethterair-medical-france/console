<?php

namespace App\Enums;

enum RecurrenceEnum: string
{
    case MONTHLY = 'monthly';
    case QUARTERLY = 'quarterly';
    case WEEKLY = 'weekly';
    case TWO_WEEK = 'two_week';
    case YEARLY = 'yearly';
    case TWO_YEAR = 'two_year';
    case FIVE_YEAR = 'five_year';
}
