<?php

namespace App\View\Components\Form;

use Illuminate\View\Component;

class InputPasswordMetter extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(public string $name,public string $label, public bool $required = false)
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.form.input-password-metter');
    }
}
