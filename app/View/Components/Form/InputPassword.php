<?php

namespace App\View\Components\Form;

use Illuminate\View\Component;

class InputPassword extends Component
{
    /**
     * Create a new component instance.
     *
     * @param string $name
     * @param string $label
     * @param bool $required
     * @param bool $meter
     */
    public function __construct(public string $name,public string $label, public bool $required = false)
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.form.input-password');
    }
}
