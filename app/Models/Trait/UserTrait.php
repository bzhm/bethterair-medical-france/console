<?php

namespace App\Models\Trait;

use Faker\Factory;
use Illuminate\Support\Str;

trait UserTrait
{
    public static function generatenic($firstname, $lastname)
    {
        return Str::limit(Str::lower($firstname), 1, '').Str::limit(Str::lower($lastname), 1, '').'-'.Factory::create('fr_FR')->randomNumber(6);
    }

    public function emailAsVerified()
    {
        return $this->email_verified_at != null;
    }
}
