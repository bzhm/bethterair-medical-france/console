<?php

namespace App\Models\Erp\Vente;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Erp\Vente\VenteInvoice
 *
 * @property int $id
 * @property string $reference Format: FCVE-YYYY-MM-99
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $vente_order_id
 * @property-read \App\Models\Erp\Vente\VenteOrder|null $order
 *
 * @method static \Illuminate\Database\Eloquent\Builder|VenteInvoice newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VenteInvoice newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VenteInvoice query()
 * @method static \Illuminate\Database\Eloquent\Builder|VenteInvoice whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VenteInvoice whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VenteInvoice whereReference($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VenteInvoice whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VenteInvoice whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VenteInvoice whereVenteOrderId($value)
 *
 * @mixin \Eloquent
 */
class VenteInvoice extends Model
{
    protected $guarded = [];

    public function order()
    {
        return $this->belongsTo(VenteOrder::class);
    }
}
