<?php

namespace App\Models\Erp\Vente;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Erp\Vente\VenteOrder
 *
 * @property int $id
 * @property string $reference Format: CMVE-YYYY-MM-99
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $estimate_id
 * @property-read \App\Models\Erp\Vente\Estimate $estimate
 *
 * @method static \Illuminate\Database\Eloquent\Builder|VenteOrder newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VenteOrder newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VenteOrder query()
 * @method static \Illuminate\Database\Eloquent\Builder|VenteOrder whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VenteOrder whereEstimateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VenteOrder whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VenteOrder whereReference($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VenteOrder whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VenteOrder whereUpdatedAt($value)
 *
 * @mixin \Eloquent
 */
class VenteOrder extends Model
{
    protected $guarded = [];

    public function estimate()
    {
        return $this->belongsTo(Estimate::class);
    }
}
