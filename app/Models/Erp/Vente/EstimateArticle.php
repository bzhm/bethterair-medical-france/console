<?php

namespace App\Models\Erp\Vente;

use App\Models\Stock\Article\Article;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Erp\Vente\EstimateArticle
 *
 * @property int $id
 * @property int $qte
 * @property float $amount
 * @property int $article_id
 * @property int $estimate_id
 * @property-read Article $article
 * @property-read \App\Models\Erp\Vente\Estimate $estimate
 *
 * @method static \Illuminate\Database\Eloquent\Builder|EstimateArticle newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EstimateArticle newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EstimateArticle query()
 * @method static \Illuminate\Database\Eloquent\Builder|EstimateArticle whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EstimateArticle whereArticleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EstimateArticle whereEstimateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EstimateArticle whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EstimateArticle whereQte($value)
 *
 * @mixin \Eloquent
 */
class EstimateArticle extends Model
{
    protected $guarded = [];

    public $timestamps = false;

    public function article()
    {
        return $this->belongsTo(Article::class);
    }

    public function estimate()
    {
        return $this->belongsTo(Estimate::class);
    }
}
