<?php

namespace App\Models\Erp\Vente;

use App\Enums\TypePaymentEnum;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Erp\Vente\VentePayment
 *
 * @property int $id
 * @property string $reference
 * @property TypePaymentEnum $type
 * @property float $amount_pay
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $vente_invoice_id
 * @property-read \App\Models\Erp\Vente\VenteInvoice|null $invoice
 *
 * @method static \Illuminate\Database\Eloquent\Builder|VentePayment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VentePayment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VentePayment query()
 * @method static \Illuminate\Database\Eloquent\Builder|VentePayment whereAmountPay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VentePayment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VentePayment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VentePayment whereReference($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VentePayment whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VentePayment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VentePayment whereVenteInvoiceId($value)
 *
 * @mixin \Eloquent
 */
class VentePayment extends Model
{
    protected $guarded = [];

    protected $casts = [
        'type' => TypePaymentEnum::class,
    ];

    public function invoice()
    {
        return $this->belongsTo(VenteInvoice::class);
    }
}
