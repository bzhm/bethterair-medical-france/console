<?php

namespace App\Models\Erp\Vente;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Erp\Vente\VenteDelivery
 *
 * @property int $id
 * @property string $reference Format: BLVE-YYYY-MM-99
 * @property string $status
 * @property string|null $shipping_number
 * @property string|null $shipping_provider
 * @property \Illuminate\Support\Carbon|null $waiting_operator_at
 * @property \Illuminate\Support\Carbon|null $ready_at
 * @property \Illuminate\Support\Carbon|null $ship_at
 * @property \Illuminate\Support\Carbon|null $delivered_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $vente_order_id
 * @property-read \App\Models\Erp\Vente\VenteOrder|null $order
 *
 * @method static \Illuminate\Database\Eloquent\Builder|VenteDelivery newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VenteDelivery newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VenteDelivery query()
 * @method static \Illuminate\Database\Eloquent\Builder|VenteDelivery whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VenteDelivery whereDeliveredAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VenteDelivery whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VenteDelivery whereReadyAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VenteDelivery whereReference($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VenteDelivery whereShipAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VenteDelivery whereShippingNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VenteDelivery whereShippingProvider($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VenteDelivery whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VenteDelivery whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VenteDelivery whereVenteOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VenteDelivery whereWaitingOperatorAt($value)
 *
 * @mixin \Eloquent
 */
class VenteDelivery extends Model
{
    protected $guarded = [];

    protected $dates = ['created_at', 'updated_at', 'waiting_operator_at', 'ready_at', 'ship_at', 'delivered_at'];

    public function order()
    {
        return $this->belongsTo(VenteOrder::class);
    }
}
