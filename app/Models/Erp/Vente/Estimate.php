<?php

namespace App\Models\Erp\Vente;

use App\Enums\PaymentTermsEnum;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Erp\Vente\Estimate
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon $expirated_at
 * @property PaymentTermsEnum $payment_terms
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $customer_id
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Estimate newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Estimate newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Estimate query()
 * @method static \Illuminate\Database\Eloquent\Builder|Estimate whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Estimate whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Estimate whereExpiratedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Estimate whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Estimate wherePaymentTerms($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Estimate whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Estimate whereUpdatedAt($value)
 *
 * @mixin \Eloquent
 */
class Estimate extends Model
{
    protected $guarded = [];

    protected $dates = ['created_at', 'updatd_at', 'expirated_at'];

    protected $casts = [
        'payment_terms' => PaymentTermsEnum::class,
    ];
}
