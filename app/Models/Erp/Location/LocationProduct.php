<?php

namespace App\Models\Erp\Location;

use App\Models\Stock\Article\Article;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Erp\Location\LocationProduct
 *
 * @property int $id
 * @property float $price_month
 * @property int $qte
 * @property int $apport_du
 * @property float|null $amount_apport
 * @property int $article_id
 * @property int $location_id
 * @property-read Article $article
 * @property-read \App\Models\Erp\Location\Location $location
 *
 * @method static \Illuminate\Database\Eloquent\Builder|LocationProduct newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LocationProduct newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LocationProduct query()
 * @method static \Illuminate\Database\Eloquent\Builder|LocationProduct whereAmountApport($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LocationProduct whereApportDu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LocationProduct whereArticleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LocationProduct whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LocationProduct whereLocationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LocationProduct wherePriceMonth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LocationProduct whereQte($value)
 *
 * @mixin \Eloquent
 */
class LocationProduct extends Model
{
    protected $guarded = [];

    public $timestamps = false;

    public function article()
    {
        return $this->belongsTo(Article::class);
    }

    public function location()
    {
        return $this->belongsTo(Location::class);
    }
}
