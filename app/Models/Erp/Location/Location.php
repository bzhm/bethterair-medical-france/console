<?php

namespace App\Models\Erp\Location;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Erp\Location\Location
 *
 * @property int $id
 * @property string $reference Format: LC-YYYY-MM-99
 * @property string $start
 * @property string $end
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $customer_id
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Location newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Location newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Location query()
 * @method static \Illuminate\Database\Eloquent\Builder|Location whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Location whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Location whereEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Location whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Location whereReference($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Location whereStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Location whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Location whereUpdatedAt($value)
 *
 * @mixin \Eloquent
 */
class Location extends Model
{
}
