<?php

namespace App\Models\Erp\Location;

use App\Models\Erp\Vente\VenteOrder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Erp\Location\LocationOrder
 *
 * @property int $id
 * @property int $location_id
 * @property int $vente_order_id
 * @property-read \App\Models\Erp\Location\Location $location
 * @property-read VenteOrder|null $order
 *
 * @method static \Illuminate\Database\Eloquent\Builder|LocationOrder newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LocationOrder newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LocationOrder query()
 * @method static \Illuminate\Database\Eloquent\Builder|LocationOrder whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LocationOrder whereLocationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LocationOrder whereVenteOrderId($value)
 *
 * @mixin \Eloquent
 */
class LocationOrder extends Model
{
    protected $guarded = [];

    public $timestamps = false;

    public function order()
    {
        return $this->belongsTo(VenteOrder::class);
    }

    public function location()
    {
        return $this->belongsTo(Location::class);
    }
}
