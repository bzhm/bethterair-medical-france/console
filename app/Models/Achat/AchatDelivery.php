<?php

namespace App\Models\Achat;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Achat\AchatDelivery
 *
 * @property int $id
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $achat_order_id
 * @property-read \App\Models\Achat\AchatOrder|null $order
 *
 * @method static \Illuminate\Database\Eloquent\Builder|AchatDelivery newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AchatDelivery newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AchatDelivery query()
 * @method static \Illuminate\Database\Eloquent\Builder|AchatDelivery whereAchatOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AchatDelivery whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AchatDelivery whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AchatDelivery whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AchatDelivery whereUpdatedAt($value)
 *
 * @mixin \Eloquent
 */
class AchatDelivery extends Model
{
    protected $guarded = [];

    public function order()
    {
        return $this->belongsTo(AchatOrder::class);
    }
}
