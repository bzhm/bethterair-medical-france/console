<?php

namespace App\Models\Achat;

use App\Models\Stock\Article\Article;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Achat\RequestArticle
 *
 * @property int $id
 * @property int $qte
 * @property float $amount
 * @property int $article_id
 * @property int $request_id
 * @property-read Article $article
 * @property-read \App\Models\Achat\Request $request
 *
 * @method static \Illuminate\Database\Eloquent\Builder|RequestArticle newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RequestArticle newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RequestArticle query()
 * @method static \Illuminate\Database\Eloquent\Builder|RequestArticle whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RequestArticle whereArticleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RequestArticle whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RequestArticle whereQte($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RequestArticle whereRequestId($value)
 *
 * @mixin \Eloquent
 */
class RequestArticle extends Model
{
    protected $guarded = [];

    public $timestamps = false;

    public function request()
    {
        return $this->belongsTo(Request::class);
    }

    public function article()
    {
        return $this->belongsTo(Article::class);
    }
}
