<?php

namespace App\Models\Achat;

use App\Enums\TypePaymentEnum;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Achat\AchatPayment
 *
 * @property int $id
 * @property string $reference
 * @property TypePaymentEnum $type
 * @property float $amount_pay
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $achat_invoice_id
 * @property-read \App\Models\Achat\AchatInvoice|null $invoice
 *
 * @method static \Illuminate\Database\Eloquent\Builder|AchatPayment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AchatPayment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AchatPayment query()
 * @method static \Illuminate\Database\Eloquent\Builder|AchatPayment whereAchatInvoiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AchatPayment whereAmountPay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AchatPayment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AchatPayment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AchatPayment whereReference($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AchatPayment whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AchatPayment whereUpdatedAt($value)
 *
 * @mixin \Eloquent
 */
class AchatPayment extends Model
{
    protected $guarded = [];

    protected $casts = [
        'type' => TypePaymentEnum::class,
    ];

    public function invoice()
    {
        return $this->belongsTo(AchatInvoice::class);
    }
}
