<?php

namespace App\Models\Achat;

use App\Models\Stock\Fournisseur;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Achat\Request
 *
 * @property int $id
 * @property string $request_echeance
 * @property string|null $request_expected_arrival si possible calculer en prévision
 * @property int $request_confirm demande confirm fournissuer
 * @property float $amount_ht
 * @property float $amount_ttc
 * @property string $status
 * @property int $fournisseur_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read Fournisseur $fournisseur
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Request newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Request newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Request query()
 * @method static \Illuminate\Database\Eloquent\Builder|Request whereAmountHt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Request whereAmountTtc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Request whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Request whereFournisseurId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Request whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Request whereRequestConfirm($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Request whereRequestEcheance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Request whereRequestExpectedArrival($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Request whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Request whereUpdatedAt($value)
 *
 * @mixin \Eloquent
 */
class Request extends Model
{
    protected $guarded = [];

    public function fournisseur()
    {
        return $this->belongsTo(Fournisseur::class);
    }
}
