<?php

namespace App\Models\Achat;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Achat\AchatOrder
 *
 * @property int $id
 * @property string $reference Format: CMAC-YYYY-MM-99
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $request_id
 * @property-read \App\Models\Achat\Request $request
 *
 * @method static \Illuminate\Database\Eloquent\Builder|AchatOrder newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AchatOrder newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AchatOrder query()
 * @method static \Illuminate\Database\Eloquent\Builder|AchatOrder whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AchatOrder whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AchatOrder whereReference($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AchatOrder whereRequestId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AchatOrder whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AchatOrder whereUpdatedAt($value)
 *
 * @mixin \Eloquent
 */
class AchatOrder extends Model
{
    protected $guarded = [];

    public function request()
    {
        return $this->belongsTo(Request::class);
    }
}
