<?php

namespace App\Models\Achat;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Achat\AchatInvoice
 *
 * @property int $id
 * @property string $reference Format: FCAC-YYYY-MM-99
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $achat_order_id
 * @property-read \App\Models\Achat\AchatOrder|null $order
 *
 * @method static \Illuminate\Database\Eloquent\Builder|AchatInvoice newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AchatInvoice newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AchatInvoice query()
 * @method static \Illuminate\Database\Eloquent\Builder|AchatInvoice whereAchatOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AchatInvoice whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AchatInvoice whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AchatInvoice whereReference($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AchatInvoice whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AchatInvoice whereUpdatedAt($value)
 *
 * @mixin \Eloquent
 */
class AchatInvoice extends Model
{
    protected $guarded = [];

    public function order()
    {
        return $this->belongsTo(AchatOrder::class);
    }
}
