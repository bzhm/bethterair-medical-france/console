<?php

namespace App\Models\Comptabilite\Configuration;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Comptabilite\Configuration\Journaux
 *
 * @property int $id
 * @property string $code
 * @property string $libelle
 * @property string $type
 * @property int|null $plan_comptable_default
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Journaux newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Journaux newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Journaux query()
 * @method static \Illuminate\Database\Eloquent\Builder|Journaux whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Journaux whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Journaux whereLibelle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Journaux wherePlanComptableDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Journaux whereType($value)
 *
 * @mixin \Eloquent
 */
class Journaux extends Model
{
    protected $guarded = [];

    public $timestamps = false;
}
