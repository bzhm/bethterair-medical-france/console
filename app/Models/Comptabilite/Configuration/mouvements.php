<?php

namespace App\Models\Comptabilite\Configuration;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Comptabilite\Configuration\mouvements
 *
 * @property int $id
 * @property string|null $piece
 * @property string $libelle
 * @property float|null $debit
 * @property float|null $credit
 * @property int $plan_comptable_code
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $journaux_id
 * @property-read \App\Models\Comptabilite\Configuration\PlanComptable|null $compte
 * @property-read \App\Models\Comptabilite\Configuration\Journaux|null $journal
 *
 * @method static \Illuminate\Database\Eloquent\Builder|mouvements newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|mouvements newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|mouvements query()
 * @method static \Illuminate\Database\Eloquent\Builder|mouvements whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|mouvements whereCredit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|mouvements whereDebit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|mouvements whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|mouvements whereJournauxId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|mouvements whereLibelle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|mouvements wherePiece($value)
 * @method static \Illuminate\Database\Eloquent\Builder|mouvements wherePlanComptableCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|mouvements whereUpdatedAt($value)
 *
 * @mixin \Eloquent
 */
class mouvements extends Model
{
    protected $guarded = [];

    public function compte()
    {
        return $this->belongsTo(PlanComptable::class);
    }

    public function journal()
    {
        return $this->belongsTo(Journaux::class);
    }
}
