<?php

namespace App\Models\Comptabilite\Configuration;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Comptabilite\Configuration\PlanComptable
 *
 * @property int $code
 * @property string $libelle
 *
 * @method static \Illuminate\Database\Eloquent\Builder|PlanComptable newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PlanComptable newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PlanComptable query()
 * @method static \Illuminate\Database\Eloquent\Builder|PlanComptable whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PlanComptable whereLibelle($value)
 *
 * @mixin \Eloquent
 */
class PlanComptable extends Model
{
    protected $guarded = [];

    public $timestamps = false;
}
