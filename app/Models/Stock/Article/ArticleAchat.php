<?php

namespace App\Models\Stock\Article;

use App\Models\Stock\Fournisseur;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Stock\Article\ArticleAchat
 *
 * @property int $id
 * @property float $amount_ht
 * @property float $amount_ttc
 * @property int $delai
 * @property int $fournisseur_id
 * @property int $article_id
 * @property-read \App\Models\Stock\Article\Article $article
 * @property-read Fournisseur $fournisseur
 *
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleAchat newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleAchat newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleAchat query()
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleAchat whereAmountHt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleAchat whereAmountTtc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleAchat whereArticleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleAchat whereDelai($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleAchat whereFournisseurId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleAchat whereId($value)
 *
 * @mixin \Eloquent
 */
class ArticleAchat extends Model
{
    protected $guarded = [];

    public $timestamps = false;

    public function fournisseur()
    {
        return $this->belongsTo(Fournisseur::class);
    }

    public function article()
    {
        return $this->belongsTo(Article::class);
    }
}
