<?php

namespace App\Models\Stock\Article;

use App\Enums\ArticleUnityEnum;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Stock\Article\Article
 *
 * @property int $id
 * @property string $title
 * @property int $sale
 * @property int $purchase
 * @property int $subscription
 * @property int $location
 * @property ArticleUnityEnum $unity
 * @property string|null $serial_number
 * @property string|null $code_ean
 * @property int|null $in_stock
 * @property int|null $cpt_vente
 * @property int|null $cpt_achat
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $article_category_id
 * @property-read \App\Models\Stock\Article\ArticleCategory|null $category
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Article newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Article newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Article query()
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereArticleCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereCodeEan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereCptAchat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereCptVente($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereInStock($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article wherePurchase($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereSale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereSerialNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereSubscription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereUnity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereUpdatedAt($value)
 *
 * @mixin \Eloquent
 *
 * @property int $active
 * @property string|null $note_disponibility
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereNoteDisponibility($value)
 */
class Article extends Model
{
    protected $guarded = [];

    protected $casts = [
        'unity' => ArticleUnityEnum::class,
    ];

    public function category()
    {
        return $this->belongsTo(ArticleCategory::class);
    }
}
