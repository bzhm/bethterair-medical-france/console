<?php

namespace App\Models\Stock\Article;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Stock\ArticleCategory
 *
 * @property int $id
 * @property string $name
 * @property int|null $parent_id
 * @property-read ArticleCategory|null $parent
 *
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleCategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleCategory whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleCategory whereParentId($value)
 *
 * @mixin \Eloquent
 *
 * @method static \Database\Factories\Stock\Article\ArticleCategoryFactory factory(...$parameters)
 */
class ArticleCategory extends Model
{
    use HasFactory;

    protected $guarded = [];

    public $timestamps = false;

    public function parent()
    {
        return $this->belongsTo(ArticleCategory::class, 'parent_id', 'id');
    }
}
