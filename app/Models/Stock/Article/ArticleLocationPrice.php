<?php

namespace App\Models\Stock\Article;

use App\Enums\CustomerLegalFormEnum;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Stock\Article\ArticleLocationPrice
 *
 * @property int $id
 * @property CustomerLegalFormEnum $group
 * @property float $price_ht
 * @property float $price_ttc
 * @property int $apport
 * @property float $amount_apport
 * @property int $article_id
 * @property-read \App\Models\Stock\Article\Article $article
 *
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleLocationPrice newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleLocationPrice newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleLocationPrice query()
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleLocationPrice whereAmountApport($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleLocationPrice whereApport($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleLocationPrice whereArticleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleLocationPrice whereGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleLocationPrice whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleLocationPrice wherePriceHt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleLocationPrice wherePriceTtc($value)
 *
 * @mixin \Eloquent
 */
class ArticleLocationPrice extends Model
{
    protected $guarded = [];

    public $timestamps = false;

    protected $casts = [
        'group' => CustomerLegalFormEnum::class,
    ];

    public function article()
    {
        return $this->belongsTo(Article::class);
    }
}
