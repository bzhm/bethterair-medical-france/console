<?php

namespace App\Models\Stock\Article;

use App\Enums\CustomerLegalFormEnum;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Stock\Article\ArticleSalePrice
 *
 * @property int $id
 * @property CustomerLegalFormEnum $group
 * @property float $price_ht
 * @property float $price_ttc
 * @property int $article_id
 * @property-read \App\Models\Stock\Article\Article $article
 *
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleSalePrice newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleSalePrice newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleSalePrice query()
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleSalePrice whereArticleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleSalePrice whereGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleSalePrice whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleSalePrice wherePriceHt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleSalePrice wherePriceTtc($value)
 *
 * @mixin \Eloquent
 */
class ArticleSalePrice extends Model
{
    protected $guarded = [];

    public $timestamps = false;

    protected $casts = [
        'group' => CustomerLegalFormEnum::class,
    ];

    public function article()
    {
        return $this->belongsTo(Article::class);
    }
}
