<?php

namespace App\Models\Stock\Article;

use App\Enums\CustomerLegalFormEnum;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Stock\Article\ArticleReccuringPrice
 *
 * @property int $id
 * @property CustomerLegalFormEnum $group
 * @property float $price_ht
 * @property float $price_ttc
 * @property int $article_id
 * @property-read \App\Models\Stock\Article\Article $article
 *
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleReccuringPrice newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleReccuringPrice newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleReccuringPrice query()
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleReccuringPrice whereArticleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleReccuringPrice whereGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleReccuringPrice whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleReccuringPrice wherePriceHt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleReccuringPrice wherePriceTtc($value)
 *
 * @mixin \Eloquent
 */
class ArticleReccuringPrice extends Model
{
    protected $guarded = [];

    public $timestamps = false;

    protected $casts = [
        'group' => CustomerLegalFormEnum::class,
    ];

    public function article()
    {
        return $this->belongsTo(Article::class);
    }
}
