<?php

namespace App\Models\Stock\Article;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Stock\Article\ArticleStock
 *
 * @property int $id
 * @property float $poids
 * @property float $volume
 * @property int $delai_customer
 * @property int $article_id
 *
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleStock newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleStock newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleStock query()
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleStock whereArticleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleStock whereDelaiCustomer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleStock whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleStock wherePoids($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleStock whereVolume($value)
 *
 * @mixin \Eloquent
 */
class ArticleStock extends Model
{
}
