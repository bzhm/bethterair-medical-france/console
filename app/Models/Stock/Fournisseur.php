<?php

namespace App\Models\Stock;

use App\Models\Core\User\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

/**
 * App\Models\Stock\Fournisseur
 *
 * @property int $id
 * @property string $name
 * @property string $address
 * @property string $postal
 * @property string $city
 * @property string $country
 * @property string $phone
 * @property string $mobile
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $user_id
 * @property-read User $user
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Fournisseur newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Fournisseur newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Fournisseur query()
 * @method static \Illuminate\Database\Eloquent\Builder|Fournisseur whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fournisseur whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fournisseur whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fournisseur whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fournisseur whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fournisseur whereMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fournisseur whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fournisseur wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fournisseur wherePostal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fournisseur whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fournisseur whereUserId($value)
 *
 * @mixin \Eloquent
 *
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 */
class Fournisseur extends Model
{
    use Notifiable;

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
