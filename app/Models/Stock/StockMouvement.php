<?php

namespace App\Models\Stock;

use App\Enums\StockMvmTypeEnum;
use App\Models\CRM\Customer;
use App\Models\Stock\Article\Article;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Stock\StockMouvement
 *
 * @property int $id
 * @property StockMvmTypeEnum $type
 * @property string $reference RO/YYYY-MM-99
 * @property int $qte
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $article_id
 * @property int|null $fournisseur_id
 * @property int|null $customer_id
 * @property-read Article $article
 * @property-read Customer|null $customer
 * @property-read \App\Models\Stock\Fournisseur|null $fournisseur
 *
 * @method static \Illuminate\Database\Eloquent\Builder|StockMouvement newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StockMouvement newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StockMouvement query()
 * @method static \Illuminate\Database\Eloquent\Builder|StockMouvement whereArticleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StockMouvement whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StockMouvement whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StockMouvement whereFournisseurId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StockMouvement whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StockMouvement whereQte($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StockMouvement whereReference($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StockMouvement whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StockMouvement whereUpdatedAt($value)
 *
 * @mixin \Eloquent
 */
class StockMouvement extends Model
{
    protected $guarded = [];

    protected $casts = [
        'type' => StockMvmTypeEnum::class,
    ];

    public function article()
    {
        return $this->belongsTo(Article::class);
    }

    public function fournisseur()
    {
        return $this->belongsTo(Fournisseur::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
}
