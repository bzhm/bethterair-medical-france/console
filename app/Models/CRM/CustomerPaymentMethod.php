<?php

namespace App\Models\CRM;

use App\Enums\ProviderPaymentEnum;
use App\Enums\TypePaymentEnum;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CRM\CustomerPaymentMethod
 *
 * @property int $id
 * @property string $label
 * @property int $default
 * @property \Illuminate\Support\Carbon|null $expiration_at
 * @property TypePaymentEnum $type
 * @property ProviderPaymentEnum $provider
 * @property string|null $provider_token
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $customer_id
 * @property-read \App\Models\CRM\Customer $customer
 *
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerPaymentMethod newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerPaymentMethod newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerPaymentMethod query()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerPaymentMethod whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerPaymentMethod whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerPaymentMethod whereDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerPaymentMethod whereExpirationAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerPaymentMethod whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerPaymentMethod whereLabel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerPaymentMethod whereProvider($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerPaymentMethod whereProviderToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerPaymentMethod whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerPaymentMethod whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerPaymentMethod whereUpdatedAt($value)
 *
 * @mixin \Eloquent
 */
class CustomerPaymentMethod extends Model
{
    protected $guarded = [];

    protected $dates = ['created_at', 'updated_at', 'expiration_at'];

    protected $casts = [
        'provider' => ProviderPaymentEnum::class,
        'type' => TypePaymentEnum::class,
    ];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
}
