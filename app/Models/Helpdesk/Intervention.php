<?php

namespace App\Models\Helpdesk;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Helpdesk\Intervention
 *
 * @property int $id
 * @property string $interNumber Format: IT-YYYY-MM-99
 * @property string $date_demande
 * @property string $date_fin_prev
 * @property string $date_fin
 * @property string $category
 * @property string $origin
 * @property string $status
 * @property string $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $customer_id
 * @property int|null $location_id
 * @property int $technicien_id
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Intervention newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Intervention newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Intervention query()
 * @method static \Illuminate\Database\Eloquent\Builder|Intervention whereCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Intervention whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Intervention whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Intervention whereDateDemande($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Intervention whereDateFin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Intervention whereDateFinPrev($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Intervention whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Intervention whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Intervention whereInterNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Intervention whereLocationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Intervention whereOrigin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Intervention whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Intervention whereTechnicienId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Intervention whereUpdatedAt($value)
 *
 * @mixin \Eloquent
 */
class Intervention extends Model
{
}
