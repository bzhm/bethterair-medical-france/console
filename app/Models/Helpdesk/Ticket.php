<?php

namespace App\Models\Helpdesk;

use App\Enums\TicketCategoryEnum;
use App\Models\CRM\Customer;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Helpdesk\Ticket
 *
 * @property int $id
 * @property string $ticketNumber Format: CSYYYYMM99
 * @property int $canBeClose
 * @property TicketCategoryEnum $category
 * @property string $lastMessageFrom
 * @property string $product selection dans la table article
 * @property int $score
 * @property string|null $contract_number uniquement si le client à un contrat de location ou de maintenance
 * @property string $status
 * @property string $sujet
 * @property string $type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $customer_id
 * @property-read Customer $customer
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket query()
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereCanBeClose($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereContractNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereLastMessageFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereProduct($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereSujet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereTicketNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereUpdatedAt($value)
 *
 * @mixin \Eloquent
 */
class Ticket extends Model
{
    protected $guarded = [];

    protected $casts = [
        'category' => TicketCategoryEnum::class,
    ];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
}
