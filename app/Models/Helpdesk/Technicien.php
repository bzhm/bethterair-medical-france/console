<?php

namespace App\Models\Helpdesk;

use App\Models\Core\User\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

/**
 * App\Models\Helpdesk\Technicien
 *
 * @property int $id
 * @property int $user_id
 * @property-read User $user
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Technicien newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Technicien newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Technicien query()
 * @method static \Illuminate\Database\Eloquent\Builder|Technicien whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Technicien whereUserId($value)
 *
 * @mixin \Eloquent
 *
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 */
class Technicien extends Model
{
    use Notifiable;

    protected $guarded = [];

    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
