<?php

namespace App\Models\Core\User;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

/**
 * App\Models\Core\User\UserInfo
 *
 * @property int $id
 * @property string $nom
 * @property string $prenom
 * @property string $phone
 * @property string $mobile
 *
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo whereMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo whereNom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo wherePrenom($value)
 *
 * @mixin \Eloquent
 *
 * @property int $user_id
 * @property-read \App\Models\Core\User\User $user
 *
 * @method static \Database\Factories\Core\User\UserInfoFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo whereUserId($value)
 *
 * @property string|null $company
 * @property int $notif_sms
 * @property int $notif_mail
 * @property-read mixed $full_name
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 *
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo whereCompany($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo whereNotifMail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo whereNotifSms($value)
 *
 * @property string|null $email
 *
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo whereEmail($value)
 */
class UserInfo extends Model
{
    use HasFactory, Notifiable;

    protected $guarded = [];

    public $timestamps = false;

    protected $appends = ['full_name'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function routeNotificationForTwilio()
    {
        return $this->mobile;
    }

    public function routeNotificationForMail()
    {
        return $this->email;
    }

    public function getFullNameAttribute()
    {
        if (! isset($this->company)) {
            return $this->prenom.' '.$this->nom;
        } else {
            return $this->company;
        }
    }
}
