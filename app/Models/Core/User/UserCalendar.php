<?php

namespace App\Models\Core\User;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Core\User\UserCalendar
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon $start
 * @property \Illuminate\Support\Carbon $end
 * @property int $all_day
 * @property string $title
 * @property mixed|null $parameters
 * @property int $user_id
 * @property-read \App\Models\Core\User\User $user
 *
 * @method static \Illuminate\Database\Eloquent\Builder|UserCalendar newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserCalendar newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserCalendar query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserCalendar whereAllDay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserCalendar whereEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserCalendar whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserCalendar whereParameters($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserCalendar whereStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserCalendar whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserCalendar whereUserId($value)
 *
 * @mixin \Eloquent
 */
class UserCalendar extends Model
{
    protected $guarded = [];

    public $timestamps = false;

    protected $dates = ['start', 'end'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
