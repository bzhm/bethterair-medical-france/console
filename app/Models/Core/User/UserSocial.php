<?php

namespace App\Models\Core\User;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Core\User\UserSocial
 *
 * @property int $id
 * @property string $github_id
 * @property string $facebook_id
 * @property string $google_id
 * @property string $twitter_id
 * @property string $paypal_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $user_id
 * @property-read \App\Models\Core\User\User $user
 *
 * @method static \Illuminate\Database\Eloquent\Builder|UserSocial newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserSocial newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserSocial query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserSocial whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserSocial whereFacebookId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserSocial whereGithubId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserSocial whereGoogleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserSocial whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserSocial wherePaypalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserSocial whereTwitterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserSocial whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserSocial whereUserId($value)
 *
 * @mixin \Eloquent
 */
class UserSocial extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
