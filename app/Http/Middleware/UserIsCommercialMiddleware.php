<?php

namespace App\Http\Middleware;

use App\Enums\UserGroupEnum;
use Closure;
use Illuminate\Http\Request;

class UserIsCommercialMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        if ($request->user()->group == UserGroupEnum::commercial) {
            return $next($request);
        } else {
            return abort(401);
        }
    }
}
