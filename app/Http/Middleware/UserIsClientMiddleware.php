<?php

namespace App\Http\Middleware;

use App\Enums\UserGroupEnum;
use Closure;
use Illuminate\Http\Request;

class UserIsClientMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        if ($request->user()->group == UserGroupEnum::client) {
            return $next($request);
        } else {
            return abort(401);
        }
    }
}
