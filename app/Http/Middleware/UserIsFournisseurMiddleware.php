<?php

namespace App\Http\Middleware;

use App\Enums\UserGroupEnum;
use Closure;
use Illuminate\Http\Request;

class UserIsFournisseurMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        if ($request->user()->group == UserGroupEnum::fournisseur) {
            return $next($request);
        } else {
            return abort(401);
        }
    }
}
