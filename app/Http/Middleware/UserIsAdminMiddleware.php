<?php

namespace App\Http\Middleware;

use App\Enums\UserGroupEnum;
use Closure;
use Illuminate\Http\Request;

class UserIsAdminMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        if ($request->user()->group == UserGroupEnum::administrateur) {
            return $next($request);
        } else {
            return abort(401);
        }
    }
}
