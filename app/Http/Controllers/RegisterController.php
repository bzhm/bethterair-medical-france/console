<?php

namespace App\Http\Controllers;

use App\Enums\UserGroupEnum;
use App\Models\Core\User\User;
use App\Models\Core\User\UserInfo;
use App\Models\CRM\Customer;
use App\Notifications\Admin\Customer\NewCustomerNotification;
use App\Notifications\Customer\WelcomeNotification;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function particulier()
    {
        return view('auth.register-part');
    }

    public function professionnel()
    {
        return view('auth.register-pro');
    }

    public function groupement()
    {
        return view('auth.register-group');
    }

    public function store(Request $request)
    {
        $request->merge(['status' => 'complete']);
        UserInfo::where('user_id', auth()->user()->id)->first()->update([
            'nom' => $request->get('lastname'),
            'prenom' => $request->get('firstname'),
            'phone' => $request->get('phone'),
            'mobile' => $request->get('mobile'),
        ]);
        Customer::where('user_id', auth()->user()->id)->first()->update($request->except('_token', 'step'));
        $user = User::find(auth()->user()->id);

        //dd(auth()->user()->customer);
        $user->customer->notify(new WelcomeNotification(auth()->user(), 'Bienvenue'));

        foreach (User::where('group', UserGroupEnum::administrateur)->get() as $admin) {
            $admin->notify(new NewCustomerNotification($user, 'Nouveau client'));
        }

        dd('OK');

        return redirect()->route('redirect');
    }

    public function logout(Request $request)
    {
        auth()->guard()->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        if ($response = auth()->logout()) {
            return $response;
        }

        return $request->wantsJson()
            ? new JsonResponse([], 204)
            : redirect('/');
    }
}
