<?php

namespace App\Http\Controllers\Api\User;

use App\Actions\Fortify\PasswordValidationRules;
use App\Enums\UserGroupEnum;
use App\Events\Core\User\UserOfflineEvent;
use App\Events\Core\User\UserOnlineEvent;
use App\Http\Controllers\Api\ApiController;
use App\Models\Core\User\User;
use Illuminate\Http\Request;
use Laravel\Fortify\Rules\Password;

class UserController extends ApiController
{
    use PasswordValidationRules;
    public function markAllRead($user_id)
    {
        try {
            User::find($user_id)->unreadNotifications->markAsRead();

            return $this->sendSuccess();
        } catch (\Exception $exception) {
            return $this->sendError(['mes' => $exception->getMessage()]);
        }
    }

    public function updateStatusOnline(User $user)
    {
        $user->update([
            'status' => 'online',
        ]);

        broadcast(new UserOnlineEvent($user));
    }

    public function updateStatusOffline(User $user)
    {
        $user->update([
            'status' => 'offline',
        ]);

        broadcast(new UserOfflineEvent($user));
    }

    public function update($user_id, Request $request)
    {
        $user = User::find($user_id);

        return match ($request->get('action')) {
            'updateProfil' => $this->updateProfil($user, $request),
            'updateSocial' => $this->updateSocial($user, $request),
            'updateEmail' => $this->updateEmail($user, $request),
            'updatePassword' => $this->updatePassword($user, $request)
        };
    }

    public function delete($user_id, Request $request)
    {
        $request->validate([
            'confirm_delete_account' => "required"
        ]);

        $user = User::find($user_id);

        $user->update(["status" => "offline"]);

        return redirect()->route('auth.logout');
    }

    private function updateProfil(User|\Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Collection|array|null $user, Request $request)
    {
        try {
            $user->update([
                'email' => $request->get('email'),
            ]);

            $user->info->update([
                'nom' => $request->get('lastname'),
                'prenom' => $request->get('firstname'),
                'email' => $request->get('email'),
                'phone' => $request->get('phone'),
                'mobile' => $request->get('mobile'),
                'notif_sms' => $request->has('notif_sms'),
                'notif_mail' => $request->has('notif_mail'),
            ]);

            return $this->sendSuccess('Votre profil à été mise à jour');
        } catch (\Exception $exception) {
            \Log::critical($exception);

            return $this->sendError(['message' => $exception->getMessage()]);
        }
    }

    private function updateSocial(User|\Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Collection|array|null $user, Request $request)
    {
        session()->push('previousLink', url()->previous());
        $provider = $request->get('provider');
        if ($request->get('btnAction') == 'activeSocial') {
            $link = route('socialite.redirect', ['driver' => $provider]);

            return $this->sendSuccess(null, ['link' => $link, 'action' => 'active']);
        } else {
            $user->social()->update([
                $provider.'_id' => null,
            ]);

            return $this->sendSuccess("Le provider: $provider à été supprimé", [
                'action' => 'inactive',
            ]);
        }
    }

    private function updateEmail(User|\Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Collection|array|null $user, Request $request)
    {
        $this->middleware(['password.confirm']);

        $user->update([
            'email' => $request->get('email')
        ]);

        $user->info->update([
            'email' => $request->get('email')
        ]);

        $user->sendEmailVerificationNotification();

        return redirect()->back()->with('success', "Votre adresse mail à bien été modifier et un mail de vérification vous à été envoyer");
    }

    private function updatePassword(User|\Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Collection|array|null $user, Request $request)
    {
        //Verify actual password
        if(\Hash::check($request->get('current_password'), $user->password)) {
            \Validator::make($request->all(), [
                "password" => $this->passwordRules()
            ]);

            $user->update([
                'password' => \Hash::make($request->get('password'))
            ]);

            return redirect()->back()->with('success', "Votre mot de passe à été mis à jours");
        } else {
            return redirect()->back()->with('error', "Mot de passe actuel incorrect !");
        }
    }
}
