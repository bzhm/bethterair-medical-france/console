<?php

namespace App\Http\Controllers\Admin\Account;

use App\Http\Controllers\Controller;
use App\Models\Core\User\User;

class ProfilController extends Controller
{
    public function index()
    {
        return view('admin.account.profil.index', [
            'user' => User::find(auth()->user()->id),
        ]);
    }
}
