<?php

namespace App\Http\Controllers\Admin\Account;

use App\Http\Controllers\Controller;

class NotifyController extends Controller
{
    public function index()
    {
        return view('admin.account.notify.index', [
            'notifications' => auth()->user()->notifications,
        ]);
    }

    public function show($notify_id)
    {
        $notification = auth()->user()->notifications()->find($notify_id);
        $notification->markAsRead();

        return view('admin.account.notify.show', compact('notification'));
    }
}
