<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Core\User\User;

class DashboardController extends Controller
{
    public function __invoke()
    {
        //dd(User::find(auth()->user()->id)->unreadNotifications()->count());
        return view('admin.dashboard');
    }
}
