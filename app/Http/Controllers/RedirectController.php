<?php

namespace App\Http\Controllers;

use App\Enums\UserGroupEnum;

class RedirectController extends Controller
{
    public function __invoke()
    {
        if (! auth()->guest()) {
            return match (auth()->user()->group) {
                default => abort(401, "Aucun accès à l'espace actuellement"),
                UserGroupEnum::administrateur => redirect()->route('admin.dashboard'),
                UserGroupEnum::client => $this->redirectClient(),
            };
        } else {
            return view('auth.login');
        }
    }

    private function redirectClient()
    {
        if (auth()->user()->customer->status == 'incomplete') {
            return match (session('register.group')) {
                'particulier' => redirect()->route('register.particulier'),
                'professionnel' => redirect()->route('register.professionnel'),
                'groupement' => redirect()->route('register.groupement'),
            };
        } else {
            return redirect()->route('client.dashboard');
        }
    }
}
