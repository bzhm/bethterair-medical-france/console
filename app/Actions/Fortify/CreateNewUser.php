<?php

namespace App\Actions\Fortify;

use App\Models\Core\User\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Laravel\Fortify\Contracts\CreatesNewUsers;

class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    /**
     * Validate and create a newly registered user.
     *
     * @param  array  $input
     * @return User|\Illuminate\Http\RedirectResponse
     */
    public function create(array $input)
    {
        //dd($input);
        if ($input['step'] == 'step1') {
            Validator::make($input, [
                'firstname' => ['required', 'string', 'max: 255'],
                'lastname' => ['required', 'string', 'max: 255'],
                'email' => ['required', 'string', 'max: 255', 'email', Rule::unique(User::class)],
                'password' => $this->passwordRules(),
            ]);
            session()->put('register', [
                'firstname' => $input['firstname'],
                'lastname' => $input['lastname'],
                'email' => $input['email'],
                'password' => Hash::make($input['password']),
                'group' => $input['type_account'],
            ]);
            $user = User::create([
                'nic' => User::generatenic($input['firstname'], $input['lastname']),
                'email' => $input['email'],
                'password' => Hash::make($input['password']),
                'group' => 'client',
                'status' => 'offline',
            ]);

            $user->info()->create([
                'nom' => $input['lastname'],
                'prenom' => $input['firstname'],
                'company' => $input['company'],
                'email' => $input['email'],
            ]);

            $increment = \App\Models\CRM\Customer::orderBy('id', 'desc')->count() != 0 ? \App\Models\CRM\Customer::orderBy('id', 'desc')->first()->id-- : '1';
            $user->customer()->create([
                'customer_code' => 'CUS-'.now()->year.'-'.$increment,
                'legalform' => $input['type_account'],
                'lastname' => $input['lastname'],
                'firstname' => $input['firstname'],
                'company' => $input['company'],
                'user_id' => $user->id,
            ]);

            return $user;
        } else {
            dd($input);
        }
    }
}
