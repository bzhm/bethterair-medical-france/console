<?php

namespace App\Actions;

use App\Models\Core\User\User;

class SocialiteAction
{
    public static function updateGithub(User $user, mixed $dataProvider)
    {
        $user->social()->update(['github_id' => $dataProvider->id]);

        return redirect(url()->previous())->with('success', 'La connexion social avec github à été établie');
    }
}
