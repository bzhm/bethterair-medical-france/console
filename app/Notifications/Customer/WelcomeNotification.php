<?php

namespace App\Notifications\Customer;

use Akibatech\FreeMobileSms\Notifications\FreeMobileChannel;
use Akibatech\FreeMobileSms\Notifications\FreeMobileMessage;
use App\Models\Core\User\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\Twilio\TwilioChannel;
use NotificationChannels\Twilio\TwilioSmsMessage;

class WelcomeNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public string $category;

    public User $user;

    public string $title;

    public string $link;

    public string $message;

    public string $messagePhone;

    public string $channelGroup;

    public function __construct(User $user, string $category)
    {
        $this->category = $category;
        $this->user = $user;
        $this->title = "Bienvenue chez Beth'terair Medical France";
        $this->message = $this->getMessage();
        $this->messagePhone = $this->getMessagePhone();
        $this->link = route('login');
        $this->channelGroup = 'client';
    }

    private function getMessage()
    {
        ob_start();
        ?>
        <p>Vous venez de créer votre compte personnel sur notre site internet <a href="https://<?= config('app.domain') ?>"><?= config('app.domain') ?></a> et nous vous en remercions.</p>
        <p>Votre identifiant: <?= $this->user->email ?> / <?= $this->user->nic ?></p>
        <p>Vous pouvez dès à présent vous connecter au site.</p>

        <?php
        return ob_get_clean();
    }

    private function getMessagePhone()
    {
        ob_start();
        ?>
        <?= $this->title; ?> <?= $this->user->info->full_name ?> https://<?= config('app.domain') ?>
        <?php
        return ob_get_clean();
    }

    private function choiceChannel()
    {
        if (config('app.env') == 'local') {
            if ($this->user->info->notif_sms) {
                return [FreeMobileChannel::class, 'database'];
            } elseif ($this->user->info->notif_sms && $this->user->info->notif_mail) {
                return [FreeMobileChannel::class, 'database', 'mail'];
            } elseif ($this->user->info->notif_mail) {
                return ['mail', 'database'];
            } else {
                return 'database';
            }
        } else {
            if ($this->user->info->notif_sms) {
                return [TwilioChannel::class, 'database'];
            }

            if ($this->user->info->notif_mail) {
                return ['mail', 'database'];
            }

            return 'database';
        }
    }

    public function via($notifiable)
    {
        return $this->choiceChannel();
    }

    public function toMail($notifiable)
    {
        $message = (new MailMessage);
        $message->subject($this->title);
        $message->view('emails.customer.welcome', [
            'content' => $this->message,
            'user' => $this->user,
        ]);
        $message->actionText = 'Me connecter';
        $message->actionUrl = $this->link;

        return $message;
    }

    public function toArray($notifiable)
    {
        return [
            'icon' => 'fa-hands-clapping',
            'color' => 'primary',
            'title' => $this->title,
            'text' => $this->message,
            'time' => now(),
            'link' => $this->link,
            'category' => $this->category,
            'models' => [$this->user],
        ];
    }

    public function toFreeMobile($notifiable)
    {
        $message = (new FreeMobileMessage());
        $message->message(strip_tags($this->messagePhone));

        return $message;
    }

    public function toTwilio($notifiable)
    {
        $message = (new TwilioSmsMessage());
        $message->content(strip_tags($this->messagePhone));

        return $message;
    }

    public function viaQueues()
    {
        return [
            'mail' => $this->channelGroup,
            'database' => $this->channelGroup,
            TwilioChannel::class => $this->channelGroup,
            FreeMobileChannel::class => $this->channelGroup,
        ];
    }

    public function withDelay()
    {
        return [
            'mail' => now()->addSeconds(15),
            'database' => now()->addSeconds(15),
            TwilioChannel::class => now()->addSeconds(60),
            FreeMobileChannel::class => now()->addSeconds(60),
        ];
    }
}
