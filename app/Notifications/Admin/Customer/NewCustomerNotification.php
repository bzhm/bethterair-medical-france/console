<?php

namespace App\Notifications\Admin\Customer;

use Akibatech\FreeMobileSms\Notifications\FreeMobileChannel;
use Akibatech\FreeMobileSms\Notifications\FreeMobileMessage;
use App\Models\Core\User\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Queue\SerializesModels;
use NotificationChannels\Twilio\TwilioChannel;
use NotificationChannels\Twilio\TwilioSmsMessage;

class NewCustomerNotification extends Notification implements ShouldQueue
{
    use Queueable, SerializesModels;

    public string $category;

    public User $user;

    public string $title;

    public string $link;

    public string $message;

    public string $messagePhone;

    public function __construct(User $user, string $category)
    {
        $this->category = $category;
        $this->user = $user;
        $this->title = 'Nouveau Client';
        $this->message = $this->getMessage();
        $this->messagePhone = $this->getMessagePhone();
        $this->link = '';
    }

    private function getMessage()
    {
        ob_start();
        ?>
        <p>Un nouveau client s'est inscrit à partir du site internet:</p>
        <table class="table table-sm">
            <tbody>
                <tr>
                    <td class="fw-bold">Type de client</td>
                    <td><?= $this->user->customer->legalform ?></td>
                </tr>
                <tr>
                    <td class="fw-bold">Identité</td>
                    <td><?= $this->user->info->full_name ?></td>
                </tr>
                <tr>
                    <td class="fw-bold">Adresse</td>
                    <td>
                        <?= $this->user->customer->address ?><br>
                        <?= $this->user->customer->postal ?> <?= $this->user->customer->city ?><br>
                    </td>
                </tr>
                <tr>
                    <td class="fw-bold">Coordonnées</td>
                    <td>
                        <i class="fa-solid fa-phone"></i>: <?= $this->user->customer->phone ?><br>
                        <i class="fa-solid fa-mobile"></i>: <?= $this->user->customer->mobile ?><br>
                        <i class="fa-solid fa-envelope"></i>: <?= $this->user->email ?><br>
                    </td>
                </tr>
            </tbody>
        </table>
        <?php
        return ob_get_clean();
    }

    private function getMessagePhone()
    {
        ob_start();
        ?>
        Un nouveau client à été ajouté à la base: <?= $this->user->info->full_name ?>
        <?php
        return ob_get_clean();
    }

    private function choiceChannel()
    {
        if (config('app.env') == 'local') {
            if ($this->user->info->notif_sms) {
                return [FreeMobileChannel::class, 'database'];
            } elseif ($this->user->info->notif_sms && $this->user->info->notif_mail) {
                return [FreeMobileChannel::class, 'database', 'mail'];
            } elseif ($this->user->info->notif_mail) {
                return ['mail', 'database'];
            } else {
                return 'database';
            }
        } else {
            if ($this->user->info->notif_sms) {
                return [TwilioChannel::class, 'database'];
            }

            if ($this->user->info->notif_mail) {
                return ['mail', 'database'];
            }

            return 'database';
        }
    }

    public function via($notifiable)
    {
        return $this->choiceChannel();
    }

    public function toMail($notifiable)
    {
        $message = (new MailMessage);
        $message->subject($this->title);
        $message->view('emails.admin.customer.new_customer', [
            'content' => $this->message,
            'user' => $this->user,
        ]);

        return $message;
    }

    public function toArray($notifiable)
    {
        return [
            'icon' => 'fa-user',
            'color' => 'primary',
            'title' => $this->title,
            'text' => $this->message,
            'time' => now(),
            'link' => $this->link,
            'category' => $this->category,
            'models' => [$this->user],
        ];
    }

    public function toFreeMobile($notifiable)
    {
        $message = (new FreeMobileMessage());
        $message->message(strip_tags($this->messagePhone));

        return $message;
    }

    public function toTwilio($notifiable)
    {
        $message = (new TwilioSmsMessage());
        $message->content(strip_tags($this->messagePhone));

        return $message;
    }

    public function viaQueues()
    {
        return [
            'mail' => 'admin',
            'database' => 'admin',
            TwilioChannel::class => 'admin',
            FreeMobileChannel::class => 'admin',
        ];
    }

    public function withDelay()
    {
        return [
            'mail' => now()->addSeconds(15),
            'database' => now()->addSeconds(15),
            TwilioChannel::class => now()->addSeconds(60),
            FreeMobileChannel::class => now()->addSeconds(60),
        ];
    }
}
