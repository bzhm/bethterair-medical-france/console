<?php

use Illuminate\Support\Facades\Route;

Route::middleware(['auth', 'group_client'])->prefix('client')->name('client.')->group(function () {
    Route::get('/', \App\Http\Controllers\Client\DashboardController::class)->name('dashboard');
});
