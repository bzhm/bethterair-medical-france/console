<?php

use Illuminate\Support\Facades\Route;

Route::prefix('auth')->group(function () {
    Route::post('login', [\App\Http\Controllers\Api\Auth\AuthController::class, 'login']);
    Route::post('signup', [\App\Http\Controllers\Api\Auth\AuthController::class, 'signup']);
    Route::middleware(['auth:api'])->group(function () {
        Route::get('logout', [AuthController::class, 'logout']);
    });
});

Route::middleware(['auth:api'])->group(function () {
    Route::prefix('user')->group(function () {
        Route::get('/', [\App\Http\Controllers\Api\User\UserController::class, 'info']);
        Route::put('{user}/online', [\App\Http\Controllers\Api\User\UserController::class, 'updateStatusOnline']);
        Route::put('{user}/offline', [\App\Http\Controllers\Api\User\UserController::class, 'updateStatusOffline']);
    });
});
