<?php

use Illuminate\Support\Facades\Route;

Route::prefix('user')->group(function () {
    Route::get('/', [\App\Http\Controllers\Api\User\UserController::class, 'list']);
    Route::get('{user_id}', [\App\Http\Controllers\Api\User\UserController::class, 'info']);
    Route::post('{user_id}', [\App\Http\Controllers\Api\User\UserController::class, 'store']);
    Route::put('{user_id}', [\App\Http\Controllers\Api\User\UserController::class, 'update']);
    Route::delete('{user_id}', [\App\Http\Controllers\Api\User\UserController::class, 'delete']);

    Route::get('{user_id}/notifications', [\App\Http\Controllers\Api\User\UserController::class, 'listNotification']);
    Route::put('{user_id}/notifications', [\App\Http\Controllers\Api\User\UserController::class, 'markAllRead']);
    Route::get('{user_id}/notifications/{notify_id}', [\App\Http\Controllers\Api\User\UserController::class, 'infoNotification']);
    Route::put('{user_id}/notifications/{notify_id}', [\App\Http\Controllers\Api\User\UserController::class, 'updateNotification']);
});
