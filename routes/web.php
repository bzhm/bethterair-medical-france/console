<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

include_once 'admin.php';
Route::get('/', function () {
    return redirect()->route('login');
});
Route::get('/redirect', \App\Http\Controllers\RedirectController::class)->name('redirect');
Route::post('webhook/deploy', [\App\Http\Controllers\DeployController::class, 'deploy']);

Route::get('/register/particulier', [\App\Http\Controllers\RegisterController::class, 'particulier'])->name('register.particulier');
Route::get('/register/professionnel', [\App\Http\Controllers\RegisterController::class, 'professionnel'])->name('register.professionnel');
Route::get('/register/groupement', [\App\Http\Controllers\RegisterController::class, 'groupement'])->name('register.groupement');
Route::post('/register/store', [\App\Http\Controllers\RegisterController::class, 'store'])->name('register.store');
Route::get('/logout', [\App\Http\Controllers\RegisterController::class, 'logout'])->name('auth.logout');
Route::get('/test', function () {
    dd(\App\Models\CRM\Customer::orderBy('id', 'desc')->count());
});

Route::get('/auth/redirect', function (Illuminate\Http\Request $request) {
    return match ($request->get('driver')) {
        'github' => \Laravel\Socialite\Facades\Socialite::driver('github')->redirect(),
        'facebook' => \Laravel\Socialite\Facades\Socialite::driver('facebook')->redirect(),
        'google' => \Laravel\Socialite\Facades\Socialite::driver('google')->redirect(),
        'twitter' => \Laravel\Socialite\Facades\Socialite::driver('twitter')->redirect(),
        'paypal' => \Laravel\Socialite\Facades\Socialite::driver('paypal')->redirect(),
    };
})->name('socialite.redirect');

Route::get('/auth/callback', function (Illuminate\Http\Request $request) {
    //return Socialite::driver('github')->redirect();
    return match ($request->get('provider')) {
        'github' => \App\Actions\SocialiteAction::updateGithub(auth()->user(), \Laravel\Socialite\Facades\Socialite::driver('github')->user()),
    };
});

Route::get('/auth/webhook', function () {
});
