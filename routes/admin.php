<?php

use Illuminate\Support\Facades\Route;

Route::middleware(['auth', 'group_admin'])->prefix('admin')->name('admin.')->group(function () {
    Route::get('/', \App\Http\Controllers\Admin\DashboardController::class)->name('dashboard');

    Route::prefix('account')->name('account.')->group(function () {
        Route::prefix('notify')->name('notify.')->group(function () {
            Route::get('/', [\App\Http\Controllers\Admin\Account\NotifyController::class, 'index'])->name('index');
            Route::get('{id}', [\App\Http\Controllers\Admin\Account\NotifyController::class, 'show'])->name('show');
        });

        Route::prefix('profil')->name('profil.')->group(function () {
            Route::get('/', [\App\Http\Controllers\Admin\Account\ProfilController::class, 'index'])->name('index');
        });
    });
});
