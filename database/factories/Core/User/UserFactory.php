<?php

namespace Database\Factories\Core\User;

use App\Enums\UserGroupEnum;
use App\Enums\UserStatusEnum;
use App\Models\Core\User\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;

class UserFactory extends Factory
{
    protected $model = User::class;

    public function definition(): array
    {
        $firstname = $this->faker->firstName();
        $lastname = $this->faker->lastName;

        return [
            'nic' => User::generatenic($firstname, $lastname),
            'email' => $firstname.'.'.$lastname.'@'.$this->faker->safeEmailDomain,
            'email_verified_at' => now(),
            'password' => Hash::make('password'),
            'group' => collect(UserGroupEnum::cases())->random(rand(0, 5)),
            'status' => collect(UserStatusEnum::cases())->random(rand(0, 1)),
        ];
    }
}
