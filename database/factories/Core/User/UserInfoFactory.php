<?php

namespace Database\Factories\Core\User;

use App\Models\Core\User\User;
use App\Models\Core\User\UserInfo;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserInfoFactory extends Factory
{
    protected $model = UserInfo::class;

    public function definition(): array
    {
        return [
            'nom' => $this->faker->firstName,
            'prenom' => $this->faker->lastName,
            'phone' => $this->faker->e164PhoneNumber,
            'mobile' => '+33'.rand(6, 7).$this->faker->randomNumber(9),
            'user_id' => User::orderBy('id', 'desc')->first()->id,
        ];
    }
}
