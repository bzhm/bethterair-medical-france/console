<?php

namespace Database\Factories\Stock\Article;

use App\Models\Stock\Article\Article;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class ArticleFactory extends Factory
{
    protected $model = Article::class;

    public function definition(): array
    {
        return [
            'title' => $this->faker->word(),
            'sale' => $this->faker->randomNumber(),
            'purchase' => $this->faker->randomNumber(),
            'subscription' => $this->faker->randomNumber(),
            'location' => $this->faker->randomNumber(),
            'unity' => $this->faker->word(),
            'serial_number' => $this->faker->word(),
            'code_ean' => $this->faker->word(),
            'in_stock' => $this->faker->randomNumber(),
            'cpt_vente' => $this->faker->randomNumber(),
            'cpt_achat' => $this->faker->randomNumber(),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'article_category_id' => $this->faker->randomNumber(),
        ];
    }
}
