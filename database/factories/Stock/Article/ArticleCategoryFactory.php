<?php

namespace Database\Factories\Stock\Article;

use App\Models\Stock\Article\ArticleCategory;
use Illuminate\Database\Eloquent\Factories\Factory;

class ArticleCategoryFactory extends Factory
{
    protected $model = ArticleCategory::class;

    public function definition(): array
    {
        return [
            'name' => $this->faker->name(),
            'parent_id' => ArticleCategory::factory(),
        ];
    }
}
