<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('location_products', function (Blueprint $table) {
            $table->id();
            $table->float('price_month');
            $table->integer('qte')->default(1);
            $table->boolean('apport_du')->default(false);
            $table->float('amount_apport')->nullable();

            $table->foreignId('article_id')
                            ->constrained()
                            ->cascadeOnUpdate()
                            ->cascadeOnDelete();

            $table->foreignId('location_id')
                            ->constrained()
                            ->cascadeOnUpdate()
                            ->cascadeOnDelete();
        });
    }

    public function down()
    {
        Schema::dropIfExists('location_products');
    }
};
