<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('interventions', function (Blueprint $table) {
            $table->id();
            $table->string('interNumber')->comment('Format: IT-YYYY-MM-99');
            $table->timestamp('date_demande');
            $table->timestamp('date_fin_prev');
            $table->timestamp('date_fin');
            $table->enum('category', [
                'curatif', // A effectuer sur le moment
                'preventif', // Vérification du matériel ou autre
            ])->default('curatif');
            $table->enum('origin', [
                'direct',
                'phone',
                'email',
            ])->default('direct');
            $table->enum('status', [
                'creating',
                'created',
                'planified',
                'progress',
                'billing',
                'ok',
                'cancel',
            ])->default('creating');
            $table->text('description');

            $table->timestamps();

            $table->foreignId('customer_id')
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();

            $table->foreignId('location_id') // Uniquement si lié à un contrat
                ->nullable()
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    public function down()
    {
        Schema::dropIfExists('interventions');
    }
};
