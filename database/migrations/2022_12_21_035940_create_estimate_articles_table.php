<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('estimate_articles', function (Blueprint $table) {
            $table->id();
            $table->integer('qte');
            $table->float('amount');

            $table->foreignId('article_id')
                            ->constrained()
                            ->cascadeOnUpdate()
                            ->cascadeOnDelete();

            $table->foreignId('estimate_id')
                            ->constrained()
                            ->cascadeOnUpdate()
                            ->cascadeOnDelete();
        });
    }

    public function down()
    {
        Schema::dropIfExists('estimate_articles');
    }
};
