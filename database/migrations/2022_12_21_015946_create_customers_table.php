<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->id();
            $table->string('customer_code')->comment('format: CUS-YYYY-00');
            $table->string('legalform')->default('particulier');
            $table->string('lastname')->nullable();
            $table->string('firstname')->nullable();

            $table->string('company')->nullable();
            $table->string('siren')->nullable();

            $table->string('address')->nullable();
            $table->string('addressbis')->nullable();
            $table->string('postal')->nullable();
            $table->string('city')->nullable();
            $table->string('country')->default('FR');
            $table->string('phone')->nullable();
            $table->string('mobile')->nullable();
            $table->float('solde')->default(0);
            $table->float('encours')->default(0);

            $table->enum('status', [
                'complete',
                'incomplete',
            ])->default('incomplete');

            $table->timestamps();

            $table->foreignId('user_id')
                            ->constrained()
                            ->cascadeOnUpdate()
                            ->cascadeOnDelete();
        });
    }

    public function down()
    {
        Schema::dropIfExists('customers');
    }
};
