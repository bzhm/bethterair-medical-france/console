<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('plan_comptables', function (Blueprint $table) {
            $table->bigInteger('code')->unsigned()->primary();
            $table->string('libelle');
        });
    }

    public function down()
    {
        Schema::dropIfExists('plan_comptables');
    }
};
