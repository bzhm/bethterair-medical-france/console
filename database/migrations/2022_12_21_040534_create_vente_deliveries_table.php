<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('vente_deliveries', function (Blueprint $table) {
            $table->id();
            $table->string('reference')->comment('Format: BLVE-YYYY-MM-99');
            $table->enum('status', [
                'created', // BL Créer
                'waiting_operator', // Préparation en cours
                'ready', // En attente de livreur
                'shipping_progress', // Livraison en cours
                'ok',
                'canceled',
            ]);
            $table->string('shipping_number')->nullable();
            $table->string('shipping_provider')->nullable();
            $table->timestamp('waiting_operator_at')->nullable();
            $table->timestamp('ready_at')->nullable();
            $table->timestamp('ship_at')->nullable();
            $table->timestamp('delivered_at')->nullable();

            $table->timestamps();
            $table->foreignId('vente_order_id')
                            ->constrained()
                            ->cascadeOnUpdate()
                            ->cascadeOnDelete();
        });
    }

    public function down()
    {
        Schema::dropIfExists('vente_deliveries');
    }
};
