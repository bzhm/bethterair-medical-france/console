<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('customer_payment_methods', function (Blueprint $table) {
            $table->id();
            $table->string('label');
            $table->boolean('default')->default(false);
            $table->timestamp('expiration_at')->nullable();
            $table->string('type');
            $table->string('provider');
            $table->string('provider_token')->nullable();
            $table->enum('status', [
                'CANCELED',
                'CANCELING',
                'CREATED',
                'CREATING',
                'ERROR',
                'EXPIRED',
                'FAILED',
                'MAINTENANCE',
                'PAUSED',
                'REJECTED',
                'REPLACED',
                'VALID',
                'VALIDATING',
            ])->default('CREATING');
            $table->timestamps();

            $table->foreignId('customer_id')
                            ->constrained()
                            ->cascadeOnUpdate()
                            ->cascadeOnDelete();
        });
    }

    public function down()
    {
        Schema::dropIfExists('customer_payment_methods');
    }
};
