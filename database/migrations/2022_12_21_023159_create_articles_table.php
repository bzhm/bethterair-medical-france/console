<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->boolean('active')->default(true);
            $table->boolean('sale')->default(true);
            $table->boolean('purchase')->default(true);
            $table->boolean('subscription')->default(false);
            $table->boolean('location')->default(false);
            $table->string('unity')->default('unite');
            $table->string('serial_number')->nullable();
            $table->string('code_ean')->nullable();
            $table->integer('in_stock')->unsigned()->nullable();
            $table->string('note_disponibility')->nullable();

            $table->integer('cpt_vente')->nullable();
            $table->integer('cpt_achat')->nullable();

            $table->timestamps();

            $table->foreignId('article_category_id')
                            ->constrained()
                            ->cascadeOnUpdate()
                            ->cascadeOnDelete();
        });
    }

    public function down()
    {
        Schema::dropIfExists('articles');
    }
};
