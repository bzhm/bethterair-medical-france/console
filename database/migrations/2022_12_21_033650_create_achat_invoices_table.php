<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('achat_invoices', function (Blueprint $table) {
            $table->id();
            $table->string('reference')->comment('Format: FCAC-YYYY-MM-99');
            $table->enum('status', [
                'creating',
                'waiting_payment',
                'ok',
            ]);

            $table->timestamps();
            $table->foreignId('achat_order_id')
                            ->constrained()
                            ->cascadeOnUpdate()
                            ->cascadeOnDelete();
        });
    }

    public function down()
    {
        Schema::dropIfExists('achat_invoices');
    }
};
