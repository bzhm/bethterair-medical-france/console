<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->id();
            $table->string('reference')->comment('Format: LC-YYYY-MM-99');
            $table->timestamp('start');
            $table->timestamp('end');
            $table->enum('status', [
                'creating',
                'waiting_id_verified',
                'waiting_signed',
                'waiting_pay',
                'ok',
                'terminated',
                'error',
            ]);

            $table->timestamps();
            $table->foreignId('customer_id')
                            ->constrained()
                            ->cascadeOnUpdate()
                            ->cascadeOnDelete();
        });
    }

    public function down()
    {
        Schema::dropIfExists('locations');
    }
};
