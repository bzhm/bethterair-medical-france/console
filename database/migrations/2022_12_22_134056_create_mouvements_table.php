<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('mouvements', function (Blueprint $table) {
            $table->id();
            $table->string('piece')->nullable();
            $table->string('libelle');
            $table->float('debit')->nullable();
            $table->float('credit')->nullable();
            $table->bigInteger('plan_comptable_code')->unsigned();
            $table->timestamps();

            $table->foreign('plan_comptable_code')
                ->on('plan_comptables')
                ->references('code')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();

            $table->foreignId('journaux_id')
                            ->constrained()
                            ->cascadeOnUpdate()
                            ->cascadeOnDelete();
        });
    }

    public function down()
    {
        Schema::dropIfExists('mouvements');
    }
};
