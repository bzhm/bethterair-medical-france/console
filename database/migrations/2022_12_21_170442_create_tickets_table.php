<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->id();
            $table->string('ticketNumber')->comment('Format: CSYYYYMM99');
            $table->boolean('canBeClose')->default(true);
            $table->string('category')->default('incident');
            $table->enum('lastMessageFrom', ['customer', 'support']);
            $table->string('product')->comment('selection dans la table article');
            $table->integer('score')->default(0);
            $table->string('contract_number')->nullable()->comment('uniquement si le client à un contrat de location ou de maintenance');
            $table->enum('status', [
                'closed',
                'open',
            ]);
            $table->string('sujet');
            $table->enum('type', [
                'criticalIntervention',
                'genericRequest',
            ]);
            $table->timestamps();

            $table->foreignId('customer_id')
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    public function down()
    {
        Schema::dropIfExists('tickets');
    }
};
