<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('journauxes', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('libelle');
            $table->enum('type', ['vente', 'achat', 'divers', 'especes', 'banque']);
            $table->bigInteger('plan_comptable_default')->unsigned()->nullable();

            $table->foreign('plan_comptable_default')
                ->on('plan_comptables')
                ->references('code')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    public function down()
    {
        Schema::dropIfExists('journauxes');
    }
};
