<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('achat_deliveries', function (Blueprint $table) {
            $table->id();
            $table->enum('status', [
                'created',
                'waiting_operator',
                'waiting',
                'ready',
                'ok',
                'canceled',
            ]);

            $table->timestamps();

            $table->foreignId('achat_order_id')
                            ->constrained()
                            ->cascadeOnUpdate()
                            ->cascadeOnDelete();
        });
    }

    public function down()
    {
        Schema::dropIfExists('achat_deliveries');
    }
};
