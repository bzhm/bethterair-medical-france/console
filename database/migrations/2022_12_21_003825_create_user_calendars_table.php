<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('user_calendars', function (Blueprint $table) {
            $table->id();
            $table->timestamp('start');
            $table->string('end');
            $table->boolean('all_day')->default(false);
            $table->string('title');
            $table->json('parameters')->nullable();

            $table->foreignId('user_id')
                            ->constrained()
                            ->cascadeOnUpdate()
                            ->cascadeOnDelete();
        });
    }

    public function down()
    {
        Schema::dropIfExists('user_calendars');
    }
};
