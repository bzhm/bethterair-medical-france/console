<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('article_location_prices', function (Blueprint $table) {
            $table->id();
            $table->string('group');
            $table->float('price_ht');
            $table->float('price_ttc');
            $table->boolean('apport');
            $table->float('amount_apport')->nullable();

            $table->foreignId('article_id')
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    public function down()
    {
        Schema::dropIfExists('article_location_prices');
    }
};
