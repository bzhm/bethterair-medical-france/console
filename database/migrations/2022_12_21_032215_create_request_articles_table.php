<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('request_articles', function (Blueprint $table) {
            $table->id();
            $table->integer('qte')->default(1);
            $table->float('amount');

            $table->foreignId('article_id')
                            ->constrained()
                            ->cascadeOnUpdate()
                            ->cascadeOnDelete();

            $table->foreignId('request_id')
                            ->constrained()
                            ->cascadeOnUpdate()
                            ->cascadeOnDelete();
        });
    }

    public function down()
    {
        Schema::dropIfExists('request_articles');
    }
};
