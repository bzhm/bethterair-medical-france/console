<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('vente_payments', function (Blueprint $table) {
            $table->id();
            $table->string('reference');
            $table->string('type')->default('virement');
            $table->float('amount_pay');

            $table->timestamps();

            $table->foreignId('vente_invoice_id')
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    public function down()
    {
        Schema::dropIfExists('vente_payments');
    }
};
