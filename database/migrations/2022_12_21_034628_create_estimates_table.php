<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('estimates', function (Blueprint $table) {
            $table->id();
            $table->timestamp('expirated_at')->default(now()->addDay()->startOfDay());
            $table->string('payment_terms')->default('immediat');
            $table->enum('status', [
                'creating',
                'validating', //en attente d'acceptation provider (eric)
                'validated', // en attente de la validation client
                'ordered',
                'canceled',
            ]);

            $table->timestamps();

            $table->foreignId('customer_id')
                            ->constrained()
                            ->cascadeOnUpdate()
                            ->cascadeOnDelete();
        });
    }

    public function down()
    {
        Schema::dropIfExists('estimates');
    }
};
