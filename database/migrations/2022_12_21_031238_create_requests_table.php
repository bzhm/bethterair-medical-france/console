<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('requests', function (Blueprint $table) {
            $table->id();
            $table->timestamp('request_echeance')->default(now()->addDays()->startOfDay());
            $table->timestamp('request_expected_arrival')->nullable()->comment('si possible calculer en prévision');
            $table->boolean('request_confirm')->default(false)->comment('demande confirm fournissuer');

            $table->float('amount_ht');
            $table->float('amount_ttc');

            $table->enum('status', [
                'CREATED',
                'SENT',
                'ORDERED',
            ])->default('CREATED');

            $table->foreignId('fournisseur_id')
                            ->constrained()
                            ->cascadeOnUpdate()
                            ->cascadeOnDelete();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('requests');
    }
};
