<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('achat_orders', function (Blueprint $table) {
            $table->id();
            $table->string('reference')->comment('Format: CMAC-YYYY-MM-99');
            $table->enum('status', [
                'created',
                'invoiced',
                'bloqued',
            ])->default('created');
            $table->timestamps();

            $table->foreignId('request_id')
                            ->constrained()
                            ->cascadeOnUpdate()
                            ->cascadeOnDelete();
        });
    }

    public function down()
    {
        Schema::dropIfExists('achat_orders');
    }
};
