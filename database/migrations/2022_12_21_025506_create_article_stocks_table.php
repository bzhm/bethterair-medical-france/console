<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('article_stocks', function (Blueprint $table) {
            $table->id();
            $table->float('poids')->default(0);
            $table->float('volume')->default(0);
            $table->integer('delai_customer')->default(0);

            $table->foreignId('article_id')
                            ->constrained()
                            ->cascadeOnUpdate()
                            ->cascadeOnDelete();
        });
    }

    public function down()
    {
        Schema::dropIfExists('article_stocks');
    }
};
