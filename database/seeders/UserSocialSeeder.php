<?php

namespace Database\Seeders;

use App\Models\Core\User\User;
use App\Models\Core\User\UserSocial;
use Illuminate\Database\Seeder;

class UserSocialSeeder extends Seeder
{
    public function run()
    {
        $users = User::all();

        foreach ($users as $user) {
            UserSocial::create(['user_id' => $user->id]);
        }
    }
}
