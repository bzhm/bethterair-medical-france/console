<?php

namespace Database\Seeders;

use App\Models\Stock\Article\Article;
use App\Models\Stock\Article\ArticleLocationPrice;
use App\Models\Stock\Article\ArticleSalePrice;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class ArticleSeeder extends Seeder
{
    public function run()
    {
        $article = Article::create([
            'title' => 'MINI KIT DOM PRO BIOLOGIC (COUVERTURE 20 à 30 M²) ',
            'sale' => 1,
            'purchase' => 1,
            'subscription' => 0,
            'location' => 1,
            'in_stock' => 0,
            'cpt_vente' => 707001,
            'cpt_achat' => 607001,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'article_category_id' => 1,
            'active' => true,
            'note_disponibility' => 'Disponible à partir du 15 Février 2023',
        ]);

        ArticleSalePrice::create([
            'group' => 'particulier',
            'price_ht' => 290.83,
            'price_ttc' => 349,
            'article_id' => $article->id,
        ]);

        ArticleSalePrice::create([
            'group' => 'professionnel',
            'price_ht' => 276.29,
            'price_ttc' => 331.54,
            'article_id' => $article->id,
        ]);

        ArticleSalePrice::create([
            'group' => 'groupement',
            'price_ht' => 193.89,
            'price_ttc' => 232.66,
            'article_id' => $article->id,
        ]);

        ArticleLocationPrice::create([
            'group' => 'professionnel',
            'price_ht' => 34.68,
            'price_ttc' => 41.61,
            'apport' => false,
            'amount_apport' => null,
            'article_id' => $article->id,
        ]);

        ArticleLocationPrice::create([
            'group' => 'professionnel',
            'price_ht' => 26.72,
            'price_ttc' => 32.06,
            'apport' => true,
            'amount_apport' => 286.82,
            'article_id' => $article->id,
        ]);
    }
}
