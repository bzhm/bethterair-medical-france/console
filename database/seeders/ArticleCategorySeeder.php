<?php

namespace Database\Seeders;

use App\Models\Stock\Article\ArticleCategory;
use Illuminate\Database\Seeder;

class ArticleCategorySeeder extends Seeder
{
    public function run()
    {
        ArticleCategory::create(['name' => 'Pour la Maison']);
        ArticleCategory::create(['name' => 'En voyageant']);
        ArticleCategory::create(['name' => 'Bureaux, Industries & Espace Commerciaux']);
        ArticleCategory::create(['name' => 'Consommables']);
    }
}
