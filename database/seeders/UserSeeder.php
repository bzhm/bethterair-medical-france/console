<?php

namespace Database\Seeders;

use App\Enums\UserGroupEnum;
use App\Enums\UserStatusEnum;
use App\Models\Core\User\User;
use App\Models\Core\User\UserInfo;
use App\Models\Helpdesk\Technicien;
use App\Models\Stock\Fournisseur;
use Faker\Factory;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    public function run()
    {
        $faker = Factory::create('fr_FR');
        User::create([
            'nic' => User::generatenic('Admin', 'Admin'),
            'email' => 'administrator@bethterairmedicalfrance.com',
            'email_verified_at' => now(),
            'password' => \Hash::make('rbU89a-4'),
            'group' => UserGroupEnum::administrateur,
            'status' => UserStatusEnum::ONLINE,
        ]);
        UserInfo::factory()->create([
            'nom' => 'Admin',
            'prenom' => 'Admin',
            'phone' => '0972422185',
            'mobile' => $faker->e164PhoneNumber,
            'email' => 'administrator@bethterairmedicalfrance.com',
        ]);

        User::create([
            'nic' => User::generatenic('Eric', 'Setbon'),
            'email' => 'direction@bethterairmedicalfrance.com',
            'email_verified_at' => now(),
            'password' => \Hash::make('mosheB55'),
            'group' => UserGroupEnum::administrateur,
            'status' => UserStatusEnum::ONLINE,
        ]);
        UserInfo::factory()->create([
            'nom' => 'Setbon',
            'prenom' => 'Eric',
            'phone' => '0972422185',
            'mobile' => '0760010555',
            'email' => 'direction@bethterairmedicalfrance.com',
        ]);

        User::create([
            'nic' => User::generatenic(fake()->firstName, fake()->lastName),
            'email' => 'commercial@bethterairmedicalfrance.com',
            'email_verified_at' => now(),
            'password' => \Hash::make('password'),
            'group' => UserGroupEnum::commercial,
            'status' => UserStatusEnum::ONLINE,
        ]);
        UserInfo::factory()->create([
            'nom' => $faker->lastName,
            'prenom' => $faker->firstName,
            'phone' => '0972422185',
            'mobile' => $faker->e164PhoneNumber,
            'email' => 'cmmercial@bethterairmedicalfrance.com',
        ]);

        User::create([
            'nic' => User::generatenic(fake()->firstName, fake()->lastName),
            'email' => 'client@bethterairmedicalfrance.com',
            'email_verified_at' => now(),
            'password' => \Hash::make('password'),
            'group' => UserGroupEnum::client,
            'status' => UserStatusEnum::ONLINE,
        ]);
        UserInfo::factory()->create([
            'nom' => $faker->lastName,
            'prenom' => $faker->firstName,
            'phone' => $faker->e164PhoneNumber,
            'mobile' => $faker->e164PhoneNumber,
            'email' => 'client@bethterairmedicalfrance.com',
        ]);

        User::create([
            'nic' => User::generatenic(fake()->firstName, fake()->lastName),
            'email' => 'revendeur@bethterairmedicalfrance.com',
            'email_verified_at' => now(),
            'password' => \Hash::make('password'),
            'group' => UserGroupEnum::revendeur,
            'status' => UserStatusEnum::ONLINE,
        ]);
        UserInfo::factory()->create([
            'nom' => $faker->lastName,
            'prenom' => $faker->firstName,
            'phone' => $faker->e164PhoneNumber,
            'mobile' => $faker->e164PhoneNumber,
            'email' => 'revendeur@bethterairmedicalfrance.com',
        ]);

        $four_user = User::create([
            'nic' => User::generatenic(fake()->firstName, fake()->lastName),
            'email' => 'fournisseur@bethterairmedicalfrance.com',
            'email_verified_at' => now(),
            'password' => \Hash::make('password'),
            'group' => UserGroupEnum::fournisseur,
            'status' => UserStatusEnum::ONLINE,
        ]);
        UserInfo::factory()->create([
            'nom' => $faker->lastName,
            'prenom' => $faker->firstName,
            'phone' => $faker->e164PhoneNumber,
            'mobile' => $faker->e164PhoneNumber,
            'email' => 'fournisseur@bethterairmedicalfrance.com',
        ]);

        Fournisseur::create([
            'name' => $faker->company,
            'address' => $faker->streetAddress,
            'postal' => $faker->postcode,
            'city' => $faker->city,
            'country' => $faker->countryCode,
            'phone' => $faker->e164PhoneNumber,
            'mobile' => $faker->e164PhoneNumber,
            'user_id' => $four_user->id,
        ]);

        $tech_user = User::create([
            'nic' => User::generatenic(fake()->firstName, fake()->lastName),
            'email' => 'technicien@bethterairmedicalfrance.com',
            'email_verified_at' => now(),
            'password' => \Hash::make('password'),
            'group' => UserGroupEnum::technicien,
            'status' => UserStatusEnum::ONLINE,
        ]);
        UserInfo::factory()->create([
            'nom' => $faker->lastName,
            'prenom' => $faker->firstName,
            'phone' => $faker->e164PhoneNumber,
            'mobile' => $faker->e164PhoneNumber,
            'email' => 'technicien@bethterairmedicalfrance.com',
        ]);
        Technicien::create([
            'user_id' => $tech_user->id,
        ]);

        for ($i = 0; $i <= 10; $i++) {
            $firstname = fake()->firstName;
            $lastname = fake()->lastName;
            $email = $firstname.'.'.$lastname.'@'.fake()->safeEmailDomain;
            $user = User::create([
                'nic' => User::generatenic(fake()->firstName, fake()->lastName),
                'email' => $email,
                'email_verified_at' => now(),
                'password' => \Hash::make('password'),
                'group' => UserGroupEnum::client,
                'status' => UserStatusEnum::ONLINE,
            ]);

            UserInfo::factory()->create([
                'nom' => $firstname,
                'prenom' => $lastname,
                'phone' => $faker->e164PhoneNumber,
                'mobile' => $faker->e164PhoneNumber,
                'user_id' => $user->id,
                'email' => $email,
            ]);
        }
    }
}
