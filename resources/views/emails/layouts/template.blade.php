<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="{{ config('app.url') }}/assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link href="{{ config('app.url') }}/assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" />
</head>
<body>
<div style="background-color:#ffffff; padding: 45px 0 34px 0; border-radius: 24px; margin:40px auto; max-width: 600px;">
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" height="auto" style="border-collapse:collapse">
        <tbody>
        <tr>
            <td align="center" valign="center" style="text-align:center; padding-bottom: 10px">
                <!--begin:Email content-->
                <div style="margin-bottom:55px; text-align:left">
                    <!--begin:Logo-->
                    <div style="margin:-10px 60px 54px 60px; text-align:center;">
                        <a href="{{ config('app.url') }}" rel="noopener" target="_blank">
                            <img alt="Logo" src="{{ config('app.url') }}/storage/logo/logo_2_minified.png" style="height: 35px">
                        </a>
                    </div>
                    <!--end:Logo-->
                    <!--begin:Text-->
                    <div style="font-size: 14px; text-align:center; font-weight: 500; margin:0 60px 38px 60px; font-family:Arial,Helvetica,sans-serif">
                        <p style="color:#181C32; font-size: 30px; font-weight:700; line-height:1.4; margin-bottom:6px">
                            @foreach ($introLines as $line)
                                {{ $line }}
                            @endforeach
                        </p>
                    </div>
                    <!--end:Text-->
                    <!--begin:Media-->
                    <div style="margin:0 60px 38px 60px">
                        <img alt="" style="width:100%" src="/metronic8/demo1/assets/media/email/img-2.png">
                    </div>
                    <!--end:Media-->
                    <!--begin:Text-->
                    <div style="font-size:14px; text-align:left; font-weight:500; margin:0 60px 33px 60px; font-family:Arial,Helvetica,sans-serif">
                        <p style="color:#181C32; font-size: 18px; font-weight:600; margin-bottom:27px">Bonjour {{ auth()->user()->info->full_name }},</p>
                        <p style="color:#3F4254; line-height:1.6">
                            @yield("content")
                            <p>
                                <?= config('app.domain') ?> vous remercie de votre confiance.<br>
                                A très bientôt sur notre site !
                            </p>
                            <div class="fs-2 fw-bold">RAPPEL DE NOS COORDONNEES :</div>
                            <address>
                                <?= config('app.domain') ?> - Service Clients
                                15 Rue d'Echampeu
                                77440 LIZY SUR OURCQ
                                FRANCE
                            </address>
                        </p>
                    </div>
                    <!--end:Text-->
                    <!--begin:Action-->
                    @isset($actionText)
                        <a href="{{ $actionUrl }}" target="_blank" style="background-color:#50cd89; border-radius:6px; display:inline-block; margin-left:60px; padding:11px 19px; color: #FFFFFF; font-size: 14px; font-weight:500; font-family:Arial,Helvetica,sans-serif">{{ $actionText }}</a>
                    @endisset
                    <!--end:Action-->
                </div>
                <!--end:Email content-->
            </td>
        </tr>
        <tr>
            <td align="center" valign="center" style="font-size: 13px; text-align:center; padding: 0 10px 10px 10px; font-weight: 500; color: #A1A5B7; font-family:Arial,Helvetica,sans-serif">
                <p style="color:#181C32; font-size: 16px; font-weight: 600; margin-bottom:9px">Tout tourne autour des clients !</p>
                <p style="margin-bottom:2px">Appelez notre numéro de service client: +33 899 648 492</p>
                <p style="margin-bottom:4px">Vous pouvez nous joindre sur
                    <a href="{{ config('app.url') }}/client/support/newRequest" rel="noopener" target="_blank" style="font-weight: 600">notre support client</a>.</p>
                <p>Du lundi au vendredi de 9h-12h30 et de 13h30 à 17h30.</p>
            </td>
        </tr>
        <tr>
            <td align="center" valign="center" style="text-align:center; padding-bottom: 20px;">
                <a href="#" style="margin-right:10px">
                    <img alt="Logo" src="/assets/media/email/icon-linkedin.svg">
                </a>
                <a href="#" style="margin-right:10px">
                    <img alt="Logo" src="/assets/media/email/icon-dribbble.svg">
                </a>
                <a href="#" style="margin-right:10px">
                    <img alt="Logo" src="/assets/media/email/icon-facebook.svg">
                </a>
                <a href="#">
                    <img alt="Logo" src="/assets/media/email/icon-twitter.svg">
                </a>
            </td>
        </tr>
        <tr>
            <td align="center" valign="center" style="font-size: 13px; padding:0 15px; text-align:center; font-weight: 500; color: #A1A5B7;font-family:Arial,Helvetica,sans-serif">
                <p>© Copyright Beth'terair Medical France.
                    <a href="https://bethterairmedicalfrance.com" rel="noopener" target="_blank" style="font-weight: 600;font-family:Arial,Helvetica,sans-serif">Me désinscire</a>&nbsp; de la newsletter.</p>
            </td>
        </tr>
        </tbody>
    </table>
</div>

</body>
</html>
