<div class="mb-10" data-kt-password-meter="true">
    <label for="{{ $name }}" class="@if($required == true) required @endif form-label">{{ $label }}</label>
    <div class="position-relative mb-3">
        <input class="form-control form-control-lg form-control-solid"
               type="password" placeholder="{{ $label }}" name="{{ $name }}" id="{{ $name }}" @if($required == true) required @endif autocomplete="off" />

        <!--begin::Visibility toggle-->
        <span class="btn btn-sm btn-icon position-absolute translate-middle top-50 end-0 me-n2"
              data-kt-password-meter-control="visibility">
                <i class="bi bi-eye-slash fs-2"></i>

                <i class="bi bi-eye fs-2 d-none"></i>
            </span>
        <!--end::Visibility toggle-->
    </div>
</div>
