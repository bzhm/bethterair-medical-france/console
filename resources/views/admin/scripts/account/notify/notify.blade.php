<script type="text/javascript">
    let tables = {}
    let elements = {
        btnAllRead: document.querySelector('.allRead')
    }
    let modals = {}
    let forms = {}
    let dataTable = {}
    let block = {}

    elements.btnAllRead.addEventListener('click', e => {
        e.preventDefault();

        elements.btnAllRead.setAttribute("data-kt-indicator", "on")

        $.ajax({
            url: `/api/user/${elements.btnAllRead.dataset.user}/notifications`,
            method: 'PUT',
            success: () => {
                elements.btnAllRead.removeAttribute('data-kt-indicator')
                toastr.success("Notification lu")

                setTimeout(() => {
                    window.location.reload()
                }, 1200)
            },
            error: err => {
                elements.btnAllRead.removeAttribute('data-kt-indicator')
                toastr.error("Erreur lors de l'exécution du script", "Erreur serveur")
                console.error(err)
            }
        })
    })
</script>
