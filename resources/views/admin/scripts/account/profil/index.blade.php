<script type="text/javascript">
    let tables = {}
    let elements = {
        btnSocial: document.querySelectorAll('.btnSocial'),
        btnChangeEmail: document.querySelector("#kt_signin_email_button")
    }
    let modals = {}
    let forms = {
        formEditProfil: document.querySelector("#formEditProfil")
    }
    let dataTable = {}
    let block = {
        blockEditProfil: new KTBlockUI(document.querySelector("#editProfil"))
    }

    elements.btnSocial.forEach(btn => {
        btn.addEventListener('click', e => {
            e.preventDefault()
            btn.setAttribute('data-kt-indicator', 'on')

            $.ajax({
                url: `/api/user/{{ $user->id }}`,
                method: "PUT",
                data: {"action": "updateSocial", "provider": btn.dataset.provider, "btnAction": btn.dataset.action},
                success: data => {
                    btn.removeAttribute('data-kt-indicator')
                    if(data.data.action === 'active') {
                        window.location.href=data.data.link
                        debugger
                    } else {
                        toastr.success(data.message)

                        setTimeout(() => {
                            window.location.reload()
                        }, 1200)
                    }
                },
                error: err => {
                    btn.removeAttribute('data-kt-indicator')
                    toastr.error(err.message)
                }
            })
        })
    })
    if(elements.btnChangeEmail) {
        elements.btnChangeEmail.addEventListener('click', e => {
            e.target.classList.toggle('d-none')
        })
    }

    $(forms.formEditProfil).on('submit', e => {
        e.preventDefault()
        let form = $(forms.formEditProfil)
        let url = form.attr('action')
        let data = form.serializeArray()

        block.blockEditProfil.block()

        $.ajax({
            url: url,
            method: 'PUT',
            data: data,
            success: data => {
                block.blockEditProfil.release()
                block.blockEditProfil.destroy()

                toastr.success(data.message)
            },
            error: err => {
                block.blockEditProfil.release()
                block.blockEditProfil.destroy()

                toastr.error(err.message)
            }
        })

    })
</script>
