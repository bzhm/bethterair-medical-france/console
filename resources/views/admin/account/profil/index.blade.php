@extends("admin.layouts.app")

@section("css")

@endsection

@section("toolbar")
    <!--begin::Toolbar-->
    <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
        <!--begin::Toolbar container-->
        <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
            <!--begin::Page title-->
            <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                <!--begin::Title-->
                <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">Mon profil</h1>
                <!--end::Title-->
                <!--begin::Breadcrumb-->
                <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                    <!--begin::Item-->
                    <li class="breadcrumb-item text-muted">
                        <a href="#" class="text-muted text-hover-primary">Administration</a>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="#" class="text-muted text-hover-primary">Mon Compte</a>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item text-muted">Mon profil</li>
                    <!--end::Item-->
                </ul>
                <!--end::Breadcrumb-->
            </div>
            <!--end::Page title-->
            <!--begin::Actions-->
            <div class="d-flex align-items-center gap-2 gap-lg-3">

            </div>
            <!--end::Actions-->
        </div>
        <!--end::Toolbar container-->
    </div>
    <!--end::Toolbar-->
@endsection

@section("content")
    <div class="card mb-5 mb-xl-10">
        <div class="card-body pt-9 pb-0">
            <!--begin::Details-->
            <div class="d-flex flex-wrap flex-sm-nowrap mb-3">
                <!--begin: Pic-->
                <div class="me-7 mb-4">
                    <div class="symbol symbol-100px symbol-lg-160px symbol-fixed position-relative">
                        <img src="{{ Gravatar::get($user->email) }}" alt="image">
                        <div class="position-absolute translate-middle bottom-0 start-100 mb-6 bg-{{ $user->status == 'online' ? 'success' : 'secondary' }} rounded-circle border border-4 border-body h-20px w-20px"></div>
                    </div>
                </div>
                <!--end::Pic-->
                <!--begin::Info-->
                <div class="flex-grow-1">
                    <!--begin::Title-->
                    <div class="d-flex justify-content-between align-items-start flex-wrap mb-2">
                        <!--begin::User-->
                        <div class="d-flex flex-column">
                            <!--begin::Name-->
                            <div class="d-flex align-items-center mb-2">
                                <a href="#" class="text-gray-900 text-hover-primary fs-2 fw-bold me-1">{{ $user->info->full_name }}</a>
                            </div>
                            <!--end::Name-->
                            <!--begin::Info-->
                            <div class="d-flex flex-wrap fw-semibold fs-6 mb-4 pe-2">
                                <a href="#" class="d-flex align-items-center text-gray-400 text-hover-primary me-5 mb-2">
                                    <i class="fa-solid fa-user-circle fs-5 me-1"></i>
                                    {{ $user->getGroup() }}
                                </a>
                                <a href="mailto:{{ $user->info->email }}" class="d-flex align-items-center text-gray-400 text-hover-primary me-5 mb-2">
                                    <i class="fa-solid fa-envelope-circle-check fs-5 me-1"></i>
                                    {{ $user->email }}
                                </a>
                                <a href="tel:{{ $user->info->phone }}" class="d-flex align-items-center text-gray-400 text-hover-primary me-5 mb-2">
                                    <i class="fa-solid fa-phone-square fs-5 me-1"></i>
                                    {{ $user->info->phone }}
                                </a>
                                <a href="tel:{{ $user->info->mobile }}" class="d-flex align-items-center text-gray-400 text-hover-primary me-5 mb-2">
                                    <i class="fa-solid fa-mobile fs-5 me-1"></i>
                                    {{ $user->info->mobile }}
                                </a>

                            </div>
                            <!--end::Info-->
                        </div>
                        <!--end::User-->
                    </div>
                    <!--end::Title-->

                </div>
                <!--end::Info-->
            </div>
            <!--end::Details-->
            <!--begin::Navs-->
            <ul class="nav nav-stretch nav-line-tabs nav-line-tabs-2x border-transparent fs-5 fw-bold">
                <!--begin::Nav item-->
                <li class="nav-item mt-2">
                    <a class="nav-link text-active-primary ms-0 me-10 py-5" data-bs-toggle="tab" href="#edit">Edition</a>
                </li>
                <!--end::Nav item-->
            </ul>
            <!--begin::Navs-->
        </div>
    </div>
    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="edit" role="tabpanel">
            <div class="card shadow-sm mb-10" id="editProfil">
                <div class="card-header">
                    <h3 class="card-title">Edition de mon profil</h3>
                    <div class="card-toolbar">
                        <!--<button type="button" class="btn btn-sm btn-light">
                            Action
                        </button>-->
                    </div>
                </div>
                <form id="formEditProfil" action="/api/user/{{ $user->id }}" method="post">
                    @csrf
                    <input type="hidden" name="action" value="updateProfil">
                    <div class="card-body">
                        <x-base.underline
                            title="Identité"
                            size-text="fs-3"
                            size="2" />
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <x-form.input
                                    name="lastname"
                                    label="Votre nom"
                                    value="{{ $user->info->nom }}"
                                    required="true" />
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <x-form.input
                                    name="firstname"
                                    label="Votre prénom"
                                    value="{{ $user->info->prenom }}"
                                    required="true" />
                            </div>
                        </div>
                        <x-base.underline
                            title="Coordonnées"
                            size-text="fs-3"
                            size="2" />
                        <div class="row">
                            <div class="col-md-4 col-sm-12">
                                <x-form.input
                                    name="email"
                                    label="Votre adresse mail"
                                    value="{{ $user->info->email }}"
                                    required="true" />
                            </div>
                            <div class="col-md-4 col-sm-12">
                                <x-form.input
                                    name="phone"
                                    label="Votre numéro de téléphone"
                                    value="{{ $user->info->phone }}" />
                            </div>
                            <div class="col-md-4 col-sm-12">
                                <x-form.input
                                    name="mobile"
                                    label="Votre numéro de portable"
                                    value="{{ $user->info->mobile }}" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-sm-12">
                                <x-form.checkbox
                                    name="notif_sms"
                                    label="Activer la notification SMS"
                                    value="1"
                                    checked="{{ $user->info->notif_sms }}" />
                            </div>
                            <div class="col-md-3 col-sm-12">
                                <x-form.checkbox
                                    name="notif_mail"
                                    label="Activer la notification EMAIL"
                                    value="1"
                                    checked="{{ $user->info->notif_mail }}" />
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <x-form.button />
                    </div>
                </form>
            </div>
            <div class="card mb-5 mb-xl-10">
                <!--begin::Card header-->
                <div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_signin_method">
                    <div class="card-title m-0">
                        <h3 class="fw-bold m-0">Moyen de connexion</h3>
                    </div>
                </div>
                <!--end::Card header-->
                <!--begin::Content-->
                <div id="kt_account_settings_signin_method" class="collapse show">
                    <!--begin::Card body-->
                    <div class="card-body border-top p-9">
                        <!--begin::Email Address-->
                        <div class="d-flex flex-wrap align-items-center">
                            <!--begin::Label-->
                            <div id="kt_signin_email">
                                <div class="fs-6 fw-bold mb-1">Adresse Mail</div>
                                <div class="fw-semibold text-gray-600">
                                    {{ $user->email }}&nbsp;
                                    @if(!$user->hasVerifiedEmail())
                                        <i class="fa-solid fa-exclamation-triangle text-warning fs-3" data-bs-toggle="tooltip" title="Adresse Mail non verifier"></i>
                                    @endif
                                </div>
                            </div>
                            <!--end::Label-->
                            <!--begin::Action-->
                            <div id="kt_signin_email_button" class="ms-auto">
                                <button class="btn btn-light btn-active-light-primary" data-bs-toggle="modal" data-bs-target="#UpdateMail">Changer l'adresse mail</button>
                            </div>
                            <!--end::Action-->
                        </div>
                        <!--end::Email Address-->
                        <!--begin::Separator-->
                        <div class="separator separator-dashed my-6"></div>
                        <!--end::Separator-->
                        <!--begin::Password-->
                        <div class="d-flex flex-wrap align-items-center mb-10">
                            <!--begin::Label-->
                            <div id="kt_signin_password">
                                <div class="fs-6 fw-bold mb-1">Password</div>
                                <div class="fw-semibold text-gray-600">************</div>
                            </div>
                            <!--end::Label-->

                            <!--begin::Action-->
                            <!--begin::Action-->
                            <div id="kt_signin_email_button" class="ms-auto">
                                <button class="btn btn-light btn-active-light-primary" data-bs-toggle="modal" data-bs-target="#UpdatePassword">Changer mon mot de passe</button>
                            </div>
                            <!--end::Action-->
                            <!--end::Action-->
                        </div>
                        <!--end::Password-->
                        <!--begin::Notice-->
                        <div class="notice d-flex bg-light-primary rounded border-primary border border-dashed p-6">
                            <!--begin::Icon-->
                            <!--begin::Svg Icon | path: icons/duotune/general/gen048.svg-->
                            <span class="svg-icon svg-icon-2tx svg-icon-primary me-4">
								<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path opacity="0.3" d="M20.5543 4.37824L12.1798 2.02473C12.0626 1.99176 11.9376 1.99176 11.8203 2.02473L3.44572 4.37824C3.18118 4.45258 3 4.6807 3 4.93945V13.569C3 14.6914 3.48509 15.8404 4.4417 16.984C5.17231 17.8575 6.18314 18.7345 7.446 19.5909C9.56752 21.0295 11.6566 21.912 11.7445 21.9488C11.8258 21.9829 11.9129 22 12.0001 22C12.0872 22 12.1744 21.983 12.2557 21.9488C12.3435 21.912 14.4326 21.0295 16.5541 19.5909C17.8169 18.7345 18.8277 17.8575 19.5584 16.984C20.515 15.8404 21 14.6914 21 13.569V4.93945C21 4.6807 20.8189 4.45258 20.5543 4.37824Z" fill="currentColor"></path>
									<path d="M10.5606 11.3042L9.57283 10.3018C9.28174 10.0065 8.80522 10.0065 8.51412 10.3018C8.22897 10.5912 8.22897 11.0559 8.51412 11.3452L10.4182 13.2773C10.8099 13.6747 11.451 13.6747 11.8427 13.2773L15.4859 9.58051C15.771 9.29117 15.771 8.82648 15.4859 8.53714C15.1948 8.24176 14.7183 8.24176 14.4272 8.53714L11.7002 11.3042C11.3869 11.6221 10.874 11.6221 10.5606 11.3042Z" fill="currentColor"></path>
								</svg>
							</span>
                            <!--end::Svg Icon-->
                            <!--end::Icon-->
                            <!--begin::Wrapper-->
                            <div class="d-flex flex-stack flex-grow-1 flex-wrap flex-md-nowrap">
                                <!--begin::Content-->
                                <div class="mb-3 mb-md-0 fw-semibold">
                                    <h4 class="text-gray-900 fw-bold">Sécurisez votre compte</h4>
                                    <div class="fs-6 text-gray-700 pe-7">L'authentification à deux facteurs ajoute une couche de sécurité supplémentaire à votre compte. Pour vous connecter, vous devrez en plus fournir un code à 6 chiffres</div>
                                </div>
                                <div class="fw-semibold mb-3">
                                    <h5 class="text-gray-900">Etat actuel de l'authentificateur</h5>
                                    <span class="badge badge-sm badge-danger">Inactif</span>
                                </div>
                                <!--end::Content-->
                                <!--begin::Action-->
                                @if($user->two_factor_confirmed)
                                    <form action="{{ route('two-factor.disable') }}" method="POST">
                                        @csrf
                                        @method("DELETE")
                                        <button class="btn btn-danger px-4 align-items-center text-nowrap">Désactiver</button>
                                    </form>
                                @elseif($user->two_factor_secret)
                                    <button class="btn btn-secondary px-4 align-items-center text-nowrap" data-bs-toggle="modal" data-bs-target="#ConfigDoubleAuth">Configurer</button>
                                @else
                                    <form action="{{ route('two-factor.enable') }}" method="POST">
                                        @csrf
                                        <button class="btn btn-success px-4 align-items-center text-nowrap">Activer</button>
                                    </form>
                                @endif

                                <!--end::Action-->
                            </div>
                            <!--end::Wrapper-->
                        </div>
                        <!--end::Notice-->
                    </div>
                    <!--end::Card body-->
                </div>
                <!--end::Content-->
            </div>
            <div class="card shadow-sm mb-10" id="editSocial">
                <div class="card-header">
                    <h3 class="card-title">Connexion Social</h3>
                    <div class="card-toolbar">
                        <!--<button type="button" class="btn btn-sm btn-light">
                            Action
                        </button>-->
                    </div>
                </div>
                <div class="card-body">
                    <div class="py-2">
                        <div class="d-flex flex-stack">
                            <div class="d-flex">
                                <img src="/assets/media/svg/brand-logos/github.svg" alt="Github Provider" class="w-30px me-6">
                                <div class="d-flex flex-column">
                                    <span class="fs-5 text-dark fw-bold">Github</span>
                                    @if($user->social->github_id != null)
                                        <span class="badge badge-sm badge-success">Connecter</span>
                                    @else
                                        <span class="badge badge-sm badge-danger">Non connecter</span>
                                    @endif
                                </div>
                            </div>
                            <div class="d-flex justify-content-end">
                                @if($user->social->github_id == null)
                                  <x-base.button
                                    text="Activer"
                                    class="btn btn-sm btn-secondary btnSocial"
                                    :datas="[['name' => 'user', 'value' => $user->id],['name' => 'provider', 'value' => 'github'], ['name' => 'action', 'value' => 'activeSocial']]" />
                                @else
                                    <x-base.button
                                        text="Désactiver"
                                        class="btn btn-sm btn-danger btnSocial"
                                        :datas="[['name' => 'user', 'value' => $user->id],['name' => 'provider', 'value' => 'github'], ['name' => 'action', 'value' => 'inactiveSocial']]" />
                                @endif
                            </div>
                        </div>
                        <div class="separator separator-dashed my-5"></div>
                        <div class="d-flex flex-stack">
                            <div class="d-flex">
                                <img src="/assets/media/svg/brand-logos/facebook-3.svg" alt="Github Provider" class="w-30px me-6">
                                <div class="d-flex flex-column">
                                    <span class="fs-5 text-dark fw-bold">Facebook</span>
                                    @if($user->social->facebook_id != null)
                                        <span class="badge badge-sm badge-success">Connecter</span>
                                    @else
                                        <span class="badge badge-sm badge-danger">Non connecter</span>
                                    @endif
                                </div>
                            </div>
                            <div class="d-flex justify-content-end">
                                @if($user->social->facebook_id == null)
                                    <x-base.button
                                        text="Activer"
                                        class="btn btn-sm btn-secondary btnSocial"
                                        :datas="[['name' => 'user', 'value' => $user->id],['name' => 'provider', 'value' => 'facebook'], ['name' => 'action', 'value' => 'activeSocial']]" />
                                @else
                                    <x-base.button
                                        text="Désactiver"
                                        class="btn btn-sm btn-danger btnSocial"
                                        :datas="[['name' => 'user', 'value' => $user->id],['name' => 'provider', 'value' => 'facebook'], ['name' => 'action', 'value' => 'inactiveSocial']]" />
                                @endif
                            </div>
                        </div>
                        <div class="separator separator-dashed my-5"></div>
                        <div class="d-flex flex-stack">
                            <div class="d-flex">
                                <img src="/assets/media/svg/brand-logos/google-icon.svg" alt="Github Provider" class="w-30px me-6">
                                <div class="d-flex flex-column">
                                    <span class="fs-5 text-dark fw-bold">Google</span>
                                    @if($user->social->google_id != null)
                                        <span class="badge badge-sm badge-success">Connecter</span>
                                    @else
                                        <span class="badge badge-sm badge-danger">Non connecter</span>
                                    @endif
                                </div>
                            </div>
                            <div class="d-flex justify-content-end">
                                @if($user->social->google_id == null)
                                    <x-base.button
                                        text="Activer"
                                        class="btn btn-sm btn-secondary btnSocial"
                                        :datas="[['name' => 'user', 'value' => $user->id],['name' => 'provider', 'value' => 'google'], ['name' => 'action', 'value' => 'activeSocial']]" />
                                @else
                                    <x-base.button
                                        text="Désactiver"
                                        class="btn btn-sm btn-danger btnSocial"
                                        :datas="[['name' => 'user', 'value' => $user->id],['name' => 'provider', 'value' => 'google'], ['name' => 'action', 'value' => 'inactiveSocial']]" />
                                @endif
                            </div>
                        </div>
                        <div class="separator separator-dashed my-5"></div>
                        <div class="d-flex flex-stack">
                            <div class="d-flex">
                                <img src="/assets/media/svg/brand-logos/twitter-2.svg" alt="Github Provider" class="w-30px me-6">
                                <div class="d-flex flex-column">
                                    <span class="fs-5 text-dark fw-bold">Twitter</span>
                                    @if($user->social->twitter_id != null)
                                        <span class="badge badge-sm badge-success">Connecter</span>
                                    @else
                                        <span class="badge badge-sm badge-danger">Non connecter</span>
                                    @endif
                                </div>
                            </div>
                            <div class="d-flex justify-content-end">
                                @if($user->social->twitter_id == null)
                                    <x-base.button
                                        text="Activer"
                                        class="btn btn-sm btn-secondary btnSocial"
                                        :datas="[['name' => 'user', 'value' => $user->id],['name' => 'provider', 'value' => 'twitter'], ['name' => 'action', 'value' => 'activeSocial']]" />
                                @else
                                    <x-base.button
                                        text="Désactiver"
                                        class="btn btn-sm btn-danger btnSocial"
                                        :datas="[['name' => 'user', 'value' => $user->id],['name' => 'provider', 'value' => 'twitter'], ['name' => 'action', 'value' => 'inactiveSocial']]" />
                                @endif
                            </div>
                        </div>
                        <div class="separator separator-dashed my-5"></div>
                        <div class="d-flex flex-stack">
                            <div class="d-flex">
                                <img src="/assets/media/svg/payment-methods/paypal.svg" alt="Github Provider" class="w-30px me-6">
                                <div class="d-flex flex-column">
                                    <span class="fs-5 text-dark fw-bold">Paypal</span>
                                    @if($user->social->paypal_id != null)
                                        <span class="badge badge-sm badge-success">Connecter</span>
                                    @else
                                        <span class="badge badge-sm badge-danger">Non connecter</span>
                                    @endif
                                </div>
                            </div>
                            <div class="d-flex justify-content-end">
                                @if($user->social->paypal_id == null)
                                    <x-base.button
                                        text="Activer"
                                        class="btn btn-sm btn-secondary btnSocial"
                                        :datas="[['name' => 'user', 'value' => $user->id],['name' => 'provider', 'value' => 'paypal'], ['name' => 'action', 'value' => 'activeSocial']]" />
                                @else
                                    <x-base.button
                                        text="Désactiver"
                                        class="btn btn-sm btn-danger btnSocial"
                                        :datas="[['name' => 'user', 'value' => $user->id],['name' => 'provider', 'value' => 'paypal'], ['name' => 'action', 'value' => 'inactiveSocial']]" />
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <!--begin::Card header-->
                <div class="card-header border-0 cursor-pointer bg-danger" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_deactivate" aria-expanded="true" aria-controls="kt_account_deactivate">
                    <div class="card-title m-0">
                        <h3 class="fw-bold m-0 text-white">Supprimer mon compte</h3>
                    </div>
                </div>
                <!--end::Card header-->
                <!--begin::Content-->
                <div id="kt_account_settings_deactivate" class="collapse show">
                    <!--begin::Form-->
                    <form id="kt_account_deactivate_form" action="/api/user/{{ $user->id }}" method="POST" class="form fv-plugins-bootstrap5 fv-plugins-framework" novalidate="novalidate">
                        @csrf
                        @method('DELETE')
                        <!--begin::Card body-->
                        <div class="card-body border-top p-9">
                            <!--begin::Notice-->
                            <div class="notice d-flex bg-light-warning rounded border-warning border border-dashed mb-9 p-6">
                                <!--begin::Icon-->
                                <!--begin::Svg Icon | path: icons/duotune/general/gen044.svg-->
                                <span class="svg-icon svg-icon-2tx svg-icon-warning me-4">
															<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
																<rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="currentColor"></rect>
																<rect x="11" y="14" width="7" height="2" rx="1" transform="rotate(-90 11 14)" fill="currentColor"></rect>
																<rect x="11" y="17" width="2" height="2" rx="1" transform="rotate(-90 11 17)" fill="currentColor"></rect>
															</svg>
														</span>
                                <!--end::Svg Icon-->
                                <!--end::Icon-->
                                <!--begin::Wrapper-->
                                <div class="d-flex flex-stack flex-grow-1">
                                    <!--begin::Content-->
                                    <div class="fw-semibold">
                                        <h4 class="text-gray-900 fw-bold">Vous allez supprimer votre compte</h4>
                                        <div class="fs-6 text-gray-700">
                                            Cette action va supprimer uniquement l'accès à votre compte ainsi que vos préférences.<br>
                                            Vos documents commerciaux, sont toujours accessible sur simple demande au support commercial: 0899 492 648
                                        </div>
                                    </div>
                                    <!--end::Content-->
                                </div>
                                <!--end::Wrapper-->
                            </div>
                            <!--end::Notice-->
                            <!--begin::Form input row-->
                            <x-form.checkbox
                                name="confirm_delete_account"
                                value="1"
                                label="Je confirme vouloir supprimé l'accès à mon compte sur le site {{ config('app.name') }}" />
                            <!--end::Form input row-->
                        </div>
                        <!--end::Card body-->
                        <!--begin::Card footer-->
                        <div class="card-footer d-flex justify-content-end py-6 px-9">
                            <button id="kt_account_deactivate_account_submit" type="submit" class="btn btn-danger fw-semibold">Supprimer mon compte</button>
                        </div>
                        <!--end::Card footer-->
                        <input type="hidden"></form>
                    <!--end::Form-->
                </div>
                <!--end::Content-->
            </div>
        </div>
    </div>
@endsection

@section("modal")
    <div class="modal fade" tabindex="-1" id="UpdateMail">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">Changement de l'adresse Mail</h3>

                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        <i class="fa-solid fa-xmark fs-1"></i>
                    </div>
                    <!--end::Close-->
                </div>

                <form id="formUpdateMail" action="/api/user/{{ $user->id }}" method="POST">
                    @csrf
                    @method('PUT')
                    <input type="hidden" name="action" value="updateEmail">
                    <div class="modal-body">
                        <x-form.input-group
                            name="email"
                            label="Adresse Mail"
                            value="{{ $user->email }}"
                            symbol="<i class='fa-solid fa-envelope'></i>"
                            placement="right"
                            required="true" />
                    </div>
                    <div class="modal-footer">
                        <x-form.button />
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" tabindex="-1" id="UpdatePassword">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">Changement du mot de passe</h3>

                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        <i class="fa-solid fa-xmark fs-1"></i>
                    </div>
                    <!--end::Close-->
                </div>

                <form id="formUpdateMail" action="/api/user/{{ $user->id }}" method="POST">
                    @csrf
                    @method('PUT')
                    <input type="hidden" name="action" value="updatePassword">
                    <div class="modal-body">
                        <x-form.input-password
                            name="current_password"
                            label="Mot de passe actuel"
                            required="true" />

                        <x-form.input-password-metter
                            name="password"
                            label="Nouveau mot de passe"
                            required="true" />

                        <x-form.input-password
                            name="confirmation_password"
                            label="Confirmation du mot de passe"
                            required="true" />
                    </div>
                    <div class="modal-footer">
                        <x-form.button />
                    </div>
                </form>
            </div>
        </div>
    </div>
    @if($user->two_factor_secret)
    <div class="modal fade" tabindex="-1" id="ConfigDoubleAuth">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">Configuration de la double authentification</h3>

                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        <i class="fa-solid fa-xmark fs-1"></i>
                    </div>
                    <!--end::Close-->
                </div>

                <form id="formConfigDoubleAuth" action="{{ route('two-factor.confirm') }}" method="POST">
                    @csrf
                    <div class="modal-body">
                        <div class="d-flex flex-row">
                            <div class="me-5">
                                {!! $user->twoFactorQrCodeSvg() !!}
                            </div>
                            <div class="">
                                <p>Validez 2FA en scannant le code QR qui suit et en saisissant le code dans l'espace ci-dessous.</p>
                                <x-form.input-mask
                                    name="code"
                                    required="true"
                                    label="Code OTP"
                                    mask="999999" />
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <x-form.button text="Activer la double authentification"/>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @endif
@endsection

@section("script")
    @include("admin.scripts.account.profil.index")
@endsection
