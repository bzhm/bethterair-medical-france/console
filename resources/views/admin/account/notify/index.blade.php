@extends("admin.layouts.app")

@section("css")

@endsection

@section("toolbar")
    <!--begin::Toolbar-->
    <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
        <!--begin::Toolbar container-->
        <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
            <!--begin::Page title-->
            <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                <!--begin::Title-->
                <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">Mes notifications</h1>
                <!--end::Title-->
                <!--begin::Breadcrumb-->
                <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                    <!--begin::Item-->
                    <li class="breadcrumb-item text-muted">
                        <a href="#" class="text-muted text-hover-primary">Administration</a>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="#" class="text-muted text-hover-primary">Mon Compte</a>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item text-muted">Mes notifications</li>
                    <!--end::Item-->
                </ul>
                <!--end::Breadcrumb-->
            </div>
            <!--end::Page title-->
            <!--begin::Actions-->
            <div class="d-flex align-items-center gap-2 gap-lg-3">
                <x-base.button
                    text="Marquer comme lu"
                    class="btn btn-sm btn-primary allRead"
                    :datas='[["name" => "user", "value" => auth()->user()->id]]' />
            </div>
            <!--end::Actions-->
        </div>
        <!--end::Toolbar container-->
    </div>
    <!--end::Toolbar-->
@endsection

@section("content")
    @foreach($notifications as $notification)
        <a href="{{ route('admin.account.notify.show', $notification->id) }}" class="card shadow-sm text-black">
            <div class="card-header">
                <div class="card-title">
                    <h3 class="me-6">{{ $notification['data']['title'] }}</h3>
                    <span class="text-muted fs-6">{!! Str::limit($notification['data']['text'], 80, '') !!}</span>
                </div>
                <div class="card-toolbar">
                    <span class="badge badge-light me-5">{{ $notification['data']['category'] }}</span>
                    @if($notification->read_at == null)
                        <span class="badge badge-danger">Non lu</span>
                    @else
                        <span class="badge badge-success">Lu</span>
                    @endif
                </div>
            </div>
        </a>
    @endforeach
@endsection

@section("modal")

@endsection

@section("script")
    @include("admin.scripts.account.notify.notify")
@endsection
